﻿namespace EinvoiceTvan.Webs.Models
{
    public class ReponseModel
    {
        public InvoiceTvanXmlWithoutCodeModel Data { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string Code { get; set; }
    }
}
