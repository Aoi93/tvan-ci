﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace EinvoiceTvan.Webs.Models
{
    [XmlRoot(ElementName = "invoice")]
    public class invoice
    {
        [XmlElement(ElementName = "invoiceData")]
        public invoiceData invoiceData { get; set; }
       
        [XmlElement(ElementName = "systemCode")]
        public string systemCode { get; set; }

        public string IdTransaction { get; set; }

        public string X509SubjectName { get; set; }
    }



    [XmlRoot(ElementName = "invoiceData")]
    public class invoiceData
    {
        //[XmlAttribute("id")]
        //public string id { get; set; }

        [XmlElement("id")]
        public Guid Id { get; set; }

        #region 1 Thông tin chung của hóa đơn
        /// <summary>
        ///Id phát sinh của phần mềm kế toán nếu người nộp thuế phát sinh hóa đơn từ phần mềm kế toán và nhập vào hệ thống lập hóa đơn
        /// </summary>
        [XmlElement("sellerAppRecordId")]
        public string SellerAppRecordId { get; set; }

        /// <summary>
        ///Id của phát sinh của phần mềm lập hóa đơn
        /// </summary>
        [XmlElement("invoiceAppRecordId")]
        public string InvoiceAppRecordId { get; set; }

        /// <summary>
        ///Mã loại hóa đơn chỉ nhận các giá trị sau: 01GTKT, 02GTTT, 07KPTQ, 03XKNB, 04HGDL.tuân thủ theo quy định ký hiệu loại
        ///hóa đơn của Thông tư hướng dẫn thi hành nghị định số 51/2010/NĐ-CP
        ///</summary>
        [XmlElement("invoiceType")]
        public string InvoiceType { get; set; }

        /// <summary>
        ///Mã mẫu hóa đơn, tuân thủ theo quy định ký hiệu mẫu hóa đơn của Thông tư hướng dẫn thi hành nghị định số 51/2010/NĐ-CP
        ///</summary>
        [XmlElement("templateCode")]
        public string TemplateCode { get; set; }

        /// <summary>
        ///Là “Ký hiệu hóa đơn” tuân thủ theo quy tắc tạo ký hiệu hóa đơn của Thông tư hướng dẫn thi hành nghị định số 51/2010/NĐ-CP
        ///</summary>
        [XmlElement("invoiceSeries")]
        public string InvoiceSeries { get; set; }

        /// <summary>
        ///Là Số hóa đơn phát sinh từ phần mềm phát sinh hóa đơn (phần mềm kế toán, ICA, T-VAN). Tuân thủ theo quy định về “Số
        /// thứ tự hóa đơn” của Thông tư hướng dẫn thi hành nghị định số 51/2010/NĐ-CP
        ///</summary>
        [XmlElement("invoiceNumber")]
        public string InvoiceNumber { get; set; }

        /// <summary>
        ///Tên hóa đơn tuân thủ theo quy tắc đặt tên của Thông tư hướng dẫn thi hành nghị định số 51/2010/NĐ-CP
        ///</summary>
        [XmlElement("invoiceName")]
        public string InvoiceName { get; set; }

        /// <summary>
        ///Ngày lập hóa đơn được thiết lập theo Nghị định 51/2010/NĐ-CP. / Ngày phiếu xuất
        ///</summary>
        [XmlElement("invoiceIssuedDate")]
        public DateTime InvoiceIssuedDate { get; set; }

        /// <summary>
        ///Thời điểm ký duyệt hóa đơn bằng chữ ký điện tử
        ///</summary>
        [XmlElement("signedDate")]
        public DateTime? SignedDate { get; set; }

        /// <summary>
        ///Thời điểm gửi hóa đơn đi xác thực
        ///</summary>
        //[XmlElement("submittedDate")]
        //public DateTime? SubmittedDate { get; set; }

        /// <summary>
        ///Số hợp đồng liên quan đến hóa đơn
        ///</summary>
        [XmlElement("contractNumber")]
        public string ContractNumber { get; set; }

        /// <summary>
        ///Ngày hợp đồng liên quan đến hóa đơn
        ///</summary>
        [XmlElement("contractDate")]
        public DateTime? ContractDate { get; set; }

        /// <summary>
        ///Mã tiền tệ dùng cho hóa đơn có chiều dài 3 ký tự theo quy định của NHNN Việt Nam. Ví dụ: USD, VND, EUR…
        ///</summary>
        [XmlElement("currencyCode")]
        public string CurrencyCode { get; set; }

        /// <summary>
        ///Tỷ giá ngoại tệ tại thời điểm lập hóa đơn quy đổi ra VNĐ
        ///</summary>
        [XmlElement("exchangeRate")]
        public double? ExchangeRate { get; set; }

        /// <summary>
        ///Ghi chú cho hóa đơn / Nội dung điều động
        ///</summary>
        [XmlElement("invoiceNote")]
        public string InvoiceNote { get; set; }

        /// <summary>
        /// Trạng thái điều chỉnh hóa đơn:
        ///1: Hóa đơn gốc
        ///3: Hóa đơn thay thế
        ///5: Hóa đơn điều chỉnh
        ///7: Hóa đơn xóa bỏ
        ///9: Hóa đơn điều chỉnh chiết khấu Xem định nghĩa tại Phụ lục 02
        ///</summary>
        [XmlElement("adjustmentType")]
        public int AdjustmentType { get; set; }

        /// <summary>
        /// Id của hóa đơn đã xác thực gốc trong trường hợp hóa đơn là
        /// - Hóa đơn thay thế
        /// - Hóa đơn điều chỉnh
        /// - Hóa đơn xóa bỏ
        ///</summary>
        [XmlElement("originalInvoiceId")]
        public Guid? OriginalInvoiceId { get; set; }

        /// <summary>
        /// Id của hóa đơn đã xác thực gốc trong trường hợp hóa đơn là
        /// - Hóa đơn thay thế
        /// - Hóa đơn điều chỉnh
        /// - Hóa đơn xóa bỏ
        ///</summary>
        [XmlElement("additionalReferenceDesc")]
        public string AdditionalReferenceDesc { get; set; }

        /// <summary>
        ///Ngày phát sinh văn bản thỏa thuận giữa bên mua và bên bán, dùng cho hóa đơn thay thế, điều chỉnh.
        ///</summary>
        [XmlElement("additionalReferenceDate")]
        public DateTime? AdditionalReferenceDate { get; set; }
        #endregion

        #region 2 Thông tin người bán

        /// <summary>
        ///Tên (đăng ký kinh doanh trong trường hợp là doanh nghiệp) của người bán
        /// </summary>
        [XmlElement("sellerLegalName")]
        public string SellerLegalName { get; set; }

        /// <summary>
        ///Mã số thuế người bán được cấp bởi TCT Việt Nam
        /// Mẫu 1: 0312770607
        /// Mẫu 2: 0312770607-00
        /// </summary>
        [XmlElement("sellerTaxCode")]
        public string SellerTaxCode { get; set; }

        /// <summary>
        ///Địa chỉ bưu điện người bán
        /// </summary>
        [XmlElement("sellerAddressLine")]
        public string SellerAddressLine { get; set; }

        /// <summary>
        ///Mã bưu điện
        /// </summary>
        [XmlElement("sellerPostalCode")]
        public string SellerPostalCode { get; set; }

        /// <summary>
        ///Tên Quận Huyện
        /// </summary>
        [XmlElement("sellerDistrictName")]
        public string SellerDistrictName { get; set; }

        /// <summary>
        ///Tên Tỉnh/Thành phố
        /// </summary>
        [XmlElement("sellerCityName")]
        public string SellerCityName { get; set; }

        /// <summary>
        ///Mã quốc gia
        /// </summary>
        [XmlElement("sellerCountryCode")]
        public string SellerCountryCode { get; set; }

        /// <summary>
        ///Số điện thoại người bán
        /// </summary>
        [XmlElement("sellerPhoneNumber")]
        public string SellerPhoneNumber { get; set; }

        /// <summary>
        ///Số fax người bán
        /// </summary>
        [XmlElement("sellerFaxNumber")]
        public string SellerFaxNumber { get; set; }

        /// <summary>
        ///Địa chỉ thư điện tử người bán
        /// </summary>
        [XmlElement("sellerEmail")]
        public string SellerEmail { get; set; }

        /// <summary>
        ///Tên trụ sở chính ngân hàng nơi người bán mở tài khoản giao dịch
        /// </summary>
        [XmlElement("sellerBankName")]
        public string SellerBankName { get; set; }

        /// <summary>
        ///Tài khoản ngân hàng của người bán
        /// </summary>
        [XmlElement("sellerBankAccount")]
        public string SellerBankAccount { get; set; }

        /// <summary>
        ///Tên người đại diện người bán
        /// </summary>
        [XmlElement("sellerContactPersonName")]
        public string SellerContactPersonName { get; set; }

        /// <summary>
        ///Tên người ký duyệt hóa đơn bằng chữ ký điện tử
        /// </summary>
        [XmlElement("sellerSignedPersonName")]
        public string SellerSignedPersonName { get; set; }

        /// <summary>
        ///Tên người gửi hóa đơn
        /// </summary>
        [XmlElement("sellerSubmittedPersonName")]
        public string SellerSubmittedPersonName { get; set; }
        #endregion

        #region 3 Thông tin người mua

        /// <summary>
        ///Tên người mua
        /// </summary>
        [XmlElement("buyerDisplayName")]
        public string BuyerDisplayName { get; set; }

        /// <summary>
        ///Tên (đăng ký kinh doanh trong trường hợp là doanh nghiệp) của người mua
        /// </summary>
        [XmlElement("buyerLegalName")]
        public string BuyerLegalName { get; set; }

        /// <summary>
        ///Mã số thuế người mua được cấp bởi TCT Việt Nam
        ///Mẫu 1: 0312770607
        ///Mẫu 2: 0312770607-001
        /// </summary>
        [XmlElement("buyerTaxCode")]
        public string BuyerTaxCode { get; set; }

        /// <summary>
        ///Địa chỉ bưu điện người mua
        /// </summary>
        [XmlElement("buyerAddressLine")]
        public string BuyerAddressLine { get; set; }

        /// <summary>
        ///Mã bưu điện 04 (Hà Nội) 08 (HCM)
        /// </summary>
        [XmlElement("buyerPostalCode")]
        public string BuyerPostalCode { get; set; }

        /// <summary>
        ///Tên Quận Huyện
        /// </summary>
        [XmlElement("buyerDistrictName")]
        public string BuyerDistrictName { get; set; }

        /// <summary>
        ///Tên Tỉnh/Thành phố
        /// </summary>
        [XmlElement("buyerCityName")]
        public string BuyerCityName { get; set; }

        /// <summary>
        ///Mã quốc gia VN (Việt Nam)
        /// </summary>
        [XmlElement("buyerCountryCode")]
        public string BuyerCountryCode { get; set; }

        /// <summary>
        ///Số điện thoại người mua
        /// </summary>
        [XmlElement("buyerPhoneNumber")]
        public string BuyerPhoneNumber { get; set; }

        /// <summary>
        ///Số fax người mua
        /// </summary>
        [XmlElement("buyerFaxNumber")]
        public string BuyerFaxNumber { get; set; }

        /// <summary>
        ///Địa chỉ thư điện tử người mua
        /// </summary>
        [XmlElement("buyerEmail")]
        public string BuyerEmail { get; set; }

        /// <summary>
        ///Tên trụ sở chính ngân hàng nơi người mua mở tài khoản giao dịch
        /// </summary>
        [XmlElement("buyerBankName")]
        public string BuyerBankName { get; set; }

        /// <summary>
        ///Tài khoản ngân hàng của người mua
        /// </summary>
        [XmlElement("buyerBankAccount")]
        public string BuyerBankAccount { get; set; }

        #endregion

        #region 4 Thông tin thanh toán

        /// <summary>
        ///Thông tin thanh toán
        /// </summary>
        [XmlElement("payments")]
        public PaymentXmlModel Payments { get; set; }

        #endregion

        #region 5 Thông tin vận chuyển hàng hóa
        /// <summary>
        ///Thông tin vận chuyển hàng hóa
        /// </summary>
        [XmlElement("delivery")]
        public DeliveryXmlModel Delivery { get; set; }

        #endregion

        #region 6 Thông tin chi tiết hóa đơn
        /// <summary>
        ///Thông tin chi tiết hóa đơn
        /// </summary>
        [XmlArray("items")]
        [XmlArrayItem("item")]
        public List<ItemsXmlModel> Items { get; set; }
        #endregion

        #region 7 Thông tin các mức thuế và thành tiền thuế

        /// <summary>
        ///Thông tin các mức thuế và thành tiền thuế
        /// </summary>
        [XmlElement("invoiceTaxBreakdowns")]
        public List<InvoiceTaxBreakdownsXmlModel> InvoiceTaxBreakdowns { get; set; }

        #endregion

        #region 8 Thông tin tóm tắt hóa đơn

        /// <summary>
        ///Tổng thành tiền cộng gộp của tất cả các dòng hóa đơn chưa bao gồm VAT.
        ///Hóa đơn thường: Tổng tiền HHDV trên các dòng HĐ
        ///Hóa đơn điều chỉnh: Tổng tiền điều chỉnh của các dòng HĐ.
        ///</summary>
        [XmlElement("sumOfTotalLineAmountWithoutVat")]
        public decimal SumOfTotalLineAmountWithoutVat { get; set; }

        /// <summary>
        ///Tổng tiền hóa đơn chưa bao gồm VAT
        ///Hóa đơn thường: Tổng tiền HHDV trên các dòng HĐ và các khoản tăng/giảm khác trên toàn HĐ
        ///Hóa đơn điều chỉnh: Tổng tiền điều chỉnh của các dòng HĐ và các khoản tăng/giảm khác trên toàn HĐ.
        ///</summary>
        [XmlElement("totalAmountWithoutVat")]
        public decimal? TotalAmountWithoutVat { get; set; }

        /// <summary>
        ///Tổng tiền thuế trên toàn hóa đơn
        /// Hóa đơn thường: Tổng tiền VAT trên các dòng HĐ và các khoản thuế khác trên toàn HĐ
        ///Hóa đơn điều chỉnh: Tổng tiền VAT điều chỉnh của các dòng HĐ và các khoản tăng/giảm VAT khác trên toàn HĐ
        ///</summary>
        [XmlElement("totalVatAmount")]
        public decimal? TotalVatAmount { get; set; }

        /// <summary>
        ///Tổng tiền trên hóa đơn đã bao gồm VAT
        ///Hóa đơn thường: Tổng tiền HHDV trên các dòng HĐ và các khoản tăng/giảm khác trên toàn HĐ đã bao gồm cả VAT
        ///Hóa đơn điều chỉnh: Tổng tiền điều chỉnh của các dòng HĐ và các khoản tăng/giảm khác trên toàn HĐ đã bao gồm cả VAT
        ///</summary>
        [XmlElement("totalAmountWithVat")]
        public decimal? TotalAmountWithVat { get; set; }

        /// <summary>
        ///Tổng tiền ngoại tệ của hóa đơn đã bao gồm VAT
        ///Hóa đơn thường: Tổng tiền HHDV trên các dòng HĐ và các khoản tăng/giảm khác trên toàn HĐ đã bao gồm cả VAT
        ///Hóa đơn điều chỉnh: Tổng tiền điều chỉnh của các dòng HĐ và các khoản tăng/giảm khác trên toàn HĐ đã bao gồm cả VAT
        ///</summary>
        [XmlElement("totalAmountWithVatFrn")]
        public decimal? TotalAmountWithVatFrn { get; set; }

        /// <summary>
        ///Số tiền hóa đơn bao gồm VAT viết bằng chữ
        ///</summary>
        [XmlElement("totalAmountWithVatInWords")]
        public string TotalAmountWithVatInWords { get; set; }

        /// <summary>
        ///Trường nhận biết tổng tiền hóa đơn bao gồm VAT tăng giảm(Hóa đơn điều chỉnh):
        ///- Hóa đơn thường: null
        ///- True: tăng
        ///- False: Giảm
        ///</summary>
        [XmlElement("isTotalAmountPos")]
        public bool? IsTotalAmountPos { get; set; }

        /// <summary>
        ///Trường nhận biết tổng tiền thuế hóa đơn tăng giảm(Hóa đơn điều chỉnh):
        ///- Hóa đơn thường: null
        ///- True: tăng
        ///- False: Giảm
        ///</summary>
        [XmlElement("isTotalVatAmountPos")]
        public bool? IsTotalVatAmountPos { get; set; }

        /// <summary>
        ///Trường nhận biết tổng tiền hóa đơn chưa bao gồm VAT tăng giảm (Hóa đơn điều chỉnh):
        ///- Hóa đơn thường: null
        ///- True: tăng
        ///- False: Giảm
        ///</summary>
        [XmlElement("isTotalAmtWithoutVatPos")]
        public bool? IsTotalAmtWithoutVatPos { get; set; }

        /// <summary>
        ///Tổng tiền chiết khấu trên toàn hóa đơn trước khi tính thuế.
        ///Chú ý:Khi tính chiết khấu, toàn hóa đơn chỉ sử dụng một mức thuế.
        ///</summary>
        [XmlElement("discountAmount")]
        public decimal? DiscountAmount { get; set; }

        /// <summary>
        ///Trường nhận biết tổng tiền chiết khấu tăng (dương) hay giảm(âm)
        ///False: giảm
        ///True: tăng
        ///Hóa đơn thường: giá trị này luôn là False.
        ///Hóa đơn điều chỉnh: giá trị này có thể là
        /// True/False tuy vào <inv:discountAmount/> là tăng hoặc giảm.
        ///</summary>
        [XmlElement("isDiscountAmtPos")]
        public bool? IsDiscountAmtPos { get; set; }
        #endregion
    }


    [XmlRoot(ElementName = "Signature")]
    public class Signature
    {
        [XmlElement("SignatureValue")]
        public string SignatureValue { get; set; }
    }


    /// <summary>
    /// 
    /// </summary>
    [XmlType("payments")]
    public class PaymentXmlModel
    {
        /// <summary>
        ///Phương thức thanh toán
        ///</summary>
        [XmlElement("paymentMethodName")]
        public string PaymentMethodName { get; set; }

        /// <summary>
        ///Ghi chú thanh toán
        ///</summary>
        [XmlElement("paymentNote")]
        public string PaymentNote { get; set; }

        /// <summary>
        ///Tổng tiền thanh toán
        ///</summary>
        [XmlElement("paymentAmount")]
        public decimal PaymentAmount { get; set; }

        /// <summary>
        ///Ngày tới hạn thanh toán
        ///</summary>
        [XmlElement("paymentDueDate")]
        public DateTime? PaymentDueDate { get; set; }

        /// <summary>
        ///Điều khoản thanh toán
        ///</summary>
        [XmlElement("paymentTerm")]
        public string PaymentTerm { get; set; }

        /// <summary>
        ///Tên ngân hàng thanh toán trong trường hợp thanh toán bằng chuyển khoản
        ///</summary>
        [XmlElement("bankName")]
        public string BankName { get; set; }

        /// <summary>
        ///Tài khoản ngân hàng thanh toán
        ///</summary>
        [XmlElement("bankAccountName")]
        public string BankAccountName { get; set; }
    }

    /// <summary>
    /// Thông tin vận chuyển hàng hóa
    /// </summary>
    [XmlType("delivery")]
    public class DeliveryXmlModel
    {
        /// <summary>
        ///Số đơn vận /Lệnh điều động số
        /// </summary>
        [XmlElement("deliveryOrderNumber")]
        public string DeliveryOrderNumber { get; set; }

        /// <summary>
        ///Ngày xuất lệnh giao hàng /Ngày điều động
        /// </summary>
        [XmlElement("deliveryOrderDate")]
        public DateTime? DeliveryOrderDate { get; set; }

        /// <summary>
        ///Người ra lệnh điều động giao hàng / Người/Đơn vị đều động
        /// </summary>
        [XmlElement("deliveryOrderBy")]
        public string DeliveryOrderBy { get; set; }

        /// <summary>
        ///Người thực hiện giao hàng hoặc tên đơn vị vận chuyển / Tên người vận truyển
        /// </summary>
        [XmlElement("deliveryBy")]
        public string DeliveryBy { get; set; }

        /// <summary>
        ///Phương thức vận chuyển / Phương tiện vận chuyển
        /// </summary>
        [XmlElement("transportationMethod")]
        public string TransportationMethod { get; set; }

        /// <summary>
        /// Địa điểm nhận hàng
        /// </summary>
        [XmlElement("placeOfReceipt")]
        public string PlaceOfReceipt { get; set; }

        /// <summary>
        ///Xuất từ kho
        /// </summary>
        [XmlElement("fromWarehouseName")]
        public string FromWarehouseName { get; set; }

        /// <summary>
        ///Nhập đến kho
        /// </summary>
        [XmlElement("toWarehouseName")]
        public string ToWarehouseName { get; set; }

        /// <summary>
        ///Số công ten nơ
        /// </summary>
        [XmlElement("containerNumber")]
        public string ContainerNumber { get; set; }
    }

    /// <summary>
    /// Thông tin chi tiết hóa đơn
    /// </summary>
    [XmlType("item")]
    public class ItemsXmlModel
    {
        /// <summary>
        ///Thứ tự dòng hóa đơn, bắt đầu từ 1
        /// </summary>
        [XmlElement("lineNumber")]
        public int LineNumber { get; set; }

        /// <summary>
        ///Mã hàng hóa, dịch vụ
        /// </summary>
        [XmlElement("itemCode")]
        public string ItemCode { get; set; }

        /// <summary>
        ///Tên hàng hóa, dịch vụ
        /// </summary>
        [XmlElement("itemName")]
        public string ItemName { get; set; }

        /// <summary>
        ///Mã đơn vị tính
        /// </summary>
        [XmlElement("unitCode")]
        public string UnitCode { get; set; }

        /// <summary>
        ///Tên đơn vị tính hàng hóa, dịch vụ
        /// </summary>
        [XmlElement("unitName")]
        public string UnitName { get; set; }

        /// <summary>
        ///Đơn giá
        /// </summary>
        [XmlElement("unitPrice")]
        public decimal UnitPrice { get; set; }

        /// <summary>
        ///Số lượng
        /// </summary>
        [XmlElement("quantity")]
        public double? Quantity { get; set; }

        /// <summary>
        ///Số lượng thực xuất
        /// </summary>
        [XmlElement("quantityActualExport")]
        public double? QuantityActualExport { get; set; }

        /// <summary>
        ///Số lượng thực nhập
        /// </summary>
        [XmlElement("quantityActualImported")]
        public double? QuantityActualImported { get; set; }

        /// <summary>
        ///Hóa đơn thường: Là tổng tiền hàng hóa dịch vụ chưa có VAT Hóa đơn điều chỉnh: 
        /// Là tổng tiền phần điều chỉnh của hàng hóa dịch vụ chưa có VAT
        /// </summary>
        [XmlElement("itemTotalAmountWithoutVat")]
        public decimal ItemTotalAmountWithoutVat { get; set; }

        /// <summary>
        ///Thuế suất của hàng hóa, dịch vụ
        /// </summary>
        [XmlElement("vatPercentage")]
        public decimal? VatPercentage { get; set; }

        /// <summary>
        ///Tổng tiền thuế
        /// </summary>
        [XmlElement("vatAmount")]
        public decimal VatAmount { get; set; }

        /// <summary>
        ///Cho biết loại hàng hóa dịch vụ là khuyến mãi hay không:
        /// True: hàng khuyến mãi
        /// False: hàng hóa thường
        /// </summary>
        [XmlElement("promotion")]
        public bool? Promotion { get; set; }

        /// <summary>
        ///Hóa đơn thường: có giá trị là Null.
        /// Hóa đơn điều chỉnh: Tổng giá trị tiền thuế bị điều chỉnh
        /// </summary>
        [XmlElement("adjustmentVatAmount")]
        public decimal? AdjustmentVatAmount { get; set; }

        /// <summary>
        ///Hóa bình thường: có giá trị là Null
        ///Hóa đơn điều chỉnh:
        /// - False: dòng hàng hóa dịch vụ bị điều chình giảm
        ///- True: dòng hàng hóa dịch vụ bị điều chỉnh tăng
        /// </summary>
        [XmlElement("isIncreaseItem")]
        public bool? IsIncreaseItem { get; set; }
    }

    /// <summary>
    /// Thông tin các mức thuế và thành tiền thuế
    /// </summary>
    [XmlType("invoiceTaxBreakdowns")]
    public class InvoiceTaxBreakdownsXmlModel
    {
        /// <summary>
        ///Mức thuế: khai báo giá trị như sau
        /// 0%: 0
        /// 5%: 5
        /// 10%: 10
        /// Không chịu thuế: -1
        /// Không kê khai nộp thuế:
        /// -2
        /// </summary>
        [XmlElement("vatPercentage")]
        public decimal VatPercentage { get; set; }


        /// <summary>
        ///Tổng tiền chịu thuế của mức thuế tương ứng
        /// </summary>
        [XmlElement("vatTaxableAmount")]
        public decimal VatTaxableAmount { get; set; }

        /// <summary>
        ///Tổng tiền thuế của mức thuế tương ứng
        /// </summary>
        [XmlElement("vatTaxAmount")]
        public decimal VatTaxAmount { get; set; }

        /// <summary>
        ///Hóa đơn thường: giá trị này là Null
        ///Hóa đơn điều chỉnh:
        /// - True: Tổng tiền đánh thuế dương
        ///- False: Tổng tiền đánh thuế âm
        /// </summary>
        [XmlElement("isVatTaxableAmountPos")]
        public bool? IsVatTaxableAmountPos { get; set; }

        /// <summary>
        ///Hóa đơn thường: giá trị này là Null
        ///Hóa đơn điều chỉnh:
        /// - True: Tổng tiền thuế dương
        ///- False: Tổng tiền thuế âm
        /// </summary>
        [XmlElement("isVatTaxAmountPos")]
        public bool? IsVatTaxAmountPos { get; set; }

        /// <summary>
        ///Lý do miễn giảm thuế
        /// </summary>
        [XmlElement("vatExemptionReason")]
        public string VatExemptionReason { get; set; }
    }
}