﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace EinvoiceTvan.Webs.Models
{
    public class BaseInvoiceTvanXmlModel
    {


    }


    /// <summary>
    /// Chứa các thông tin phiên bản, mã nơi gửi, mã nơi nhận, mã loại thông điệp, mã thông điệp, mã thông điệp tham chiếu, mã số thuế, số lượng.
    /// </summary>
    [XmlType("TTChung")]
    public class TTChung
    {
        /// <summary>
        /// Phiên bản của thông điệp (Trong Quy định này có giá trị là 1.0.0)
        /// Bắt buộc
        /// Dài tối đa 6 ký tự
        /// </summary>
        [XmlElement("PBan")]
        public string PBan { get; set; }

        /// <summary>
        /// Mã nơi gửi
        /// Mã nơi gửi (MNGui), mã nơi nhận (MNNhan): Được quy định đối với cơ quan thuế là TCT; 
        /// đối với T-VAN, TCKNGTT là mã MST không bao gồm dấu “-” của đơn vị.
        /// Bắt buộc
        /// Dài tối đa 13 ký tự
        /// </summary>
        [XmlElement("MNGui")]
        public string MNGui { get; set; }

        /// <summary>
        /// Mã nơi nhận
        /// Mã nơi gửi (MNGui), mã nơi nhận (MNNhan): Được quy định đối với cơ quan thuế là TCT; 
        /// đối với T-VAN, TCKNGTT là mã MST không bao gồm dấu “-” của đơn vị.
        /// Bắt buộc
        /// Dài tối đa 13 ký tự
        /// </summary>
        [XmlElement("MNNhan")]
        public string MNNhan { get; set; }

        /// <summary>
        /// Mã loại thông điệp (tham khảo phụ lục I Quyết định 635)
        /// Bắt buộc
        /// Dài tối đa 3 số
        /// </summary>
        [XmlElement("MLTDiep")]
        public int MLTDiep { get; set; }

        /// <summary>
        /// Mã thông điệp
        /// Mã thông điệp: Được sinh ra bởi hệ thống nơi gửi, đảm bảo tính duy nhất trên toàn hệ thống, có định dạng: MNGui + Y1Y2 + N1N2N3N4N5N6N7N8N9N10N11N12 trong đó: Y1, Y2 là 2 số cuối chỉ năm gửi, N1 đến N10 là các số từ 0 đến 9.
        /// Bắt buộc
        /// Dài tối đa 25 ký tự
        /// </summary>
        [XmlElement("MTDiep")]
        public string MTDiep { get; set; }

        /// <summary>
        /// Mã thông điệp tham chiếu
        /// Mã thông điệp tham chiếu: Được sinh ra đối với các thông điệp phản hồi và có giá trị là mã thông điệp của thông điệp gửi đến.
        /// KHÔNG bắt buộc
        /// Dài tối đa 25 ký tự
        /// </summary>
        [XmlElement("MTDTChieu")]
        public string MTDTChieu { get; set; }

        /// <summary>
        /// Mã số thuế (MST của NNT)
        /// Là mã số thuế của NNT có dữ liệu được gửi trong thông điệp.
        /// Bắt buộc
        /// Dài tối đa 14 ký tự
        /// </summary>
        [XmlElement("MST")]
        public string MST { get; set; }

        /// <summary>
        /// Số lượng
        /// Là tổng số lượng dữ liệu (Tổng số lượng hóa đơn không mã, tổng số lượng bảng tổng hợp dữ liệu hóa đơn không mã,...) bên trong thẻ DLieu của thông điệp
        /// Bắt buộc
        /// Dài tối đa 7 số
        /// </summary>
        [XmlElement("SLuong")]
        public string SLuong { get; set; }

    }


    /// <summary>
    /// chứa toàn bộ dữ liệu của một hóa đơn điện tử
    /// Thẻ HDon chứa thông tin dữ liệu hóa đơn và thông tin chữ ký số
    /// </summary>
    [XmlType("HDon")]
    public class HDon
    {
        /// <summary>
        /// Chứa dữ liệu hóa đơn điện tử do người bán lập:
        /// chứa các thông tin chung, nội dung chi tiết hóa đơn, dữ liệu QR Code và thông tin khác do người bán tự định nghĩa
        /// </summary>
        [XmlElement("DLHDon")]
        public DLHDon DLHDon { get; set; }

        /// <summary>
        /// Chứa dữ liệu mã của cơ quan thuế, do cơ quan thuế tạo ra và được áp dụng đối với hóa đơn điện tử có mã 
        /// (Thẻ này không có trong hóa đơn của NNT gửi cơ quan thuế để cấp mã)
        /// </summary>
        [XmlElement("MCCQT")]
        public string MCCQT { get; set; }

        /// <summary>
        /// chứa thông tin chữ ký số, bao gồm chữ ký số của người bán, người mua, cơ quan thuế và các chữ ký số khác (nếu có).
        /// </summary>
        [XmlElement("DSCKS")]
        public DSCKS DSCKS { get; set; }
    }


    /// <summary>
    /// Chứa dữ liệu hóa đơn điện tử do người bán lập:
    /// </summary>
    [XmlType("DLHDon")]
    public class DLHDon
    {
        /// <summary>
        /// Chứa các thông tin chung của hóa đơn (Tên hóa đơn, ký hiệu mẫu số, ký hiệu, số hóa đơn, thời điểm lập,...)
        /// </summary>
        [XmlElement("TTChung")]
        public TTChungDLHDon TTChung { get; set; }

        /// <summary>
        /// Chứa các thông tin nội dung hóa đơn (người bán, người mua, hàng hóa dịch vụ, thanh toán,...)
        /// </summary>
        [XmlElement("NDHDon")]
        public NDHDon NDHDon { get; set; }

        /// <summary>
        /// Dữ liệu QR Code
        /// KHÔNG bắt buộc
        /// Dài tối đa 512 ký tự
        /// </summary>
        [XmlElement("DLQRCode")]
        public string DLQRCode { get; set; }

        /// <summary>
        /// Chứa thông tin khác (nếu có)
        /// </summary>
        [XmlElement("TTKhac")]
        public TTKhac TTKhac { get; set; }
    }

    /// <summary>
    /// Thẻ HDon\DLHDon\TTChung chứa thông tin chung của hóa đơn
    /// </summary>
    //[XmlType("TTChung")]
    public class TTChungDLHDon
    {
        /// <summary>
        /// Phiên bản của thông điệp (Trong Quy định này có giá trị là 1.0.0)
        /// KHÔNG bắt buộc
        /// Dài tối đa 6 ký tự
        /// </summary>
        [XmlElement("PBan")]
        public string PBan { get; set; }

        /// <summary>
        /// Tên hóa đơn 
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// Dài tối đa 50 ký tự
        /// </summary>
        [XmlElement("THDon")]
        public string THDon { get; set; }

        /// <summary>
        /// Ký hiệu mẫu số hóa đơn
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// Dài tối đa 1 ký tự
        /// </summary>
        [XmlElement("KHMSHDon")]
        public int KHMSHDon { get; set; }

        /// <summary>
        /// Ký hiệu hóa đơn
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// Dài tối đa 6 ký tự
        /// </summary>
        [XmlElement("KHHDon")]
        public string KHHDon { get; set; }

        /// <summary>
        /// Số hóa đơn
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// Dài tối đa 8 ký tự
        /// </summary>
        [XmlElement("SHDon")]
        public int SHDon { get; set; }

        /// <summary>
        /// Thời điểm lập
        /// Bắt buộc 
        /// </summary>
        [XmlElement("TDLap")]
        public DateTime TDLap { get; set; }

        #region Trường hợp thay thế cho hóa đơn điện tử đã lập có sai sót thì hóa đơn điện tử được lập mới bổ sung các thông tin sau vào trong thẻ <TTChung/> và sau thẻ <TDLap/>:

        /// <summary>
        /// Hình thức hóa đơn bị thay thế
        /// Giá trị như sau
        /// 1 : Hóa đơn điện tử theo Nghị định số 119/2018/NĐ-CP
        /// 2 : Hóa đơn điện tử có mã xác thực của cơ quan thuế theo Quyết định số 1209/QĐ-BTC ngày 23 tháng 6 năm 2015 và Quyết định số 2660/QĐ-BTC ngày 14 tháng 12 năm 2016 của Bộ Tài chính(Hóa đơn có mã xác thực của CQT theo Nghị định số 51/2010/NĐ-CP và Nghị định số 04/2014/NĐ-CP)
        /// 3 : Các loại hóa đơn theo Nghị định số 51/2010/NĐ-CP và Nghị định số 04/2014/NĐ-CP(Trừ hóa đơn điện tử có mã xác thực của cơ quan thuế theo Quyết định số 1209/QĐ-BTC và Quyết định số 2660/QĐ-BTC)
        /// KHÔNG bắt buộc 
        /// </summary>
        [XmlElement("HTHDBTThe")]
        public int? HTHDBTThe { get; set; }

        /// <summary>
        /// Thời điểm lập hóa đơn thay thế
        /// Bắt buộc
        /// </summary>
        [XmlElement("TDLHDBTThe")]
        public DateTime TDLHDBTThe { get; set; }

        /// <summary>
        /// Ký hiệu mẫu số hóa đơn bị thay thế
        /// Tối đa 11 ký tự
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// </summary>
        [XmlElement("KHMSHDBTThe")]
        public string KHMSHDBTThe { get; set; }

        /// <summary>
        /// Ký hiệu hóa đơn bị thay thế
        /// Tối đa 8 ký tự
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// </summary>
        [XmlElement("KHHDBTThe")]
        public string KHHDBTThe { get; set; }

        /// <summary>
        ///Số hóa đơn bị thay thế
        /// Tối đa 8 ký tự
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// </summary>
        [XmlElement("SHDBTThe")]
        public string SHDBTThe { get; set; }

        #endregion

        /// <summary>
        /// Đơn vị tiền tệ
        /// Bắt buộc 
        /// </summary>
        [XmlElement("DVTTe")]
        public string DVTTe { get; set; }

        /// <summary>
        /// Tỷ giá
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// Dài tối đa 7 ký tự, trong đó tối đa 2 ký tự phần thập phân
        /// </summary>
        [XmlElement("TGia")]
        public double TGia { get; set; }

        /// <summary>
        /// Chứa các thông tin bổ sung của NNT (nếu có)
        /// Nội dung của thẻ chứa tối đa 500 ký tự.
        /// Có thể đặt thẻ <TTKhac> ở nhiều vị trí bên trong thẻ <DLHDon>. Định dạng thẻ <TTKhac> như sau
        /// KHÔNG bắt buộc
        /// </summary>
        [XmlElement("TTKhac")]
        public TTKhac TTKhac { get; set; }

    }

    /// <summary>
    /// Chứa dữ liệu chữ ký số của người bán, người mua, cơ quan thuế (đối với hóa đơn điện tử có mã) và chữ ký số khác (nếu có)
    /// </summary>
    [XmlType("DSCKS")]
    public class DSCKS
    {
        /// <summary>
        /// chứa thông tin chữ ký số người bán
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// </summary>
        [XmlElement("NBan")]
        public DSCKS_NBan NBan { get; set; }

        /// <summary>
        /// chứa thông tin chữ ký số người mua (nếu có)
        /// KHÔNG bắt buộc
        /// </summary>
        [XmlElement("NMua")]
        public DSCKS_NMua NMua { get; set; }

        /// <summary>
        /// chứa các chữ ký số khác (nếu có).
        /// </summary>
        [XmlElement("CCKSKhac")]
        public string CCKSKhac { get; set; }
    }

    public class DSCKS_NBan
    {
        //TODO:  khi có chữ ký số thì add thẻ này vào
        ///// <summary>
        ///// Chữ ký số người bán (Thực hiện theo quy định tại Điểm đ, Khoản 1, Điều 3 của Thông tư số 68/2019/TT-BTC)
        ///// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        ///// </summary>
        //[XmlElement("Signature")]
        //public string Signature { get; set; }      
    }

    public class DSCKS_NMua
    {
        //TODO:  khi có chữ ký số thì add thẻ này vào
        ///// <summary>
        ///// Chữ ký số người bán (Thực hiện theo quy định tại Điểm đ, Khoản 1, Điều 3 của Thông tư số 68/2019/TT-BTC)
        ///// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        ///// </summary>
        //[XmlElement("Signature")]
        //public string Signature { get; set; }     
    }

    /// <summary>
    /// Chứa các thông tin nội dung hóa đơn (người bán, người mua, hàng hóa dịch vụ, thanh toán,...)
    /// </summary>
    [XmlType("NDHDon")]
    public class NDHDon
    {
        /// <summary>
        /// chứa tên, địa chỉ, MST của người bán
        /// </summary>
        [XmlElement("NBan")]
        public NDHDonNBan NBan { get; set; }

        /// <summary>
        ///chứa tên, địa chỉ, MST của người mua
        /// </summary>
        [XmlElement("NMua")]
        public NDHDonNMua NMua { get; set; }

        /// <summary>
        /// chứa danh sách hàng hóa dịch vụ
        /// </summary>
        [XmlElement("DSHHDVu")]
        public DSHHDVu DSHHDVu { get; set; }

        /// <summary>
        /// chứa thông tin thanh toán của hóa đơn
        /// </summary>
        [XmlElement("TToan")]
        public TToan TToan { get; set; }
    }


    [XmlType("NBan")]
    public class NDHDonNBan
    {
        /// <summary>
        /// Tên người bán
        /// Tối đa 400 ký tự
        /// Bắt buộc
        /// </summary>
        [XmlElement("Ten")]
        public string Ten { get; set; }

        /// <summary>
        /// Mã số thuế người bán
        /// Tối đa 14 ký tự
        /// Bắt buộc
        /// </summary>
        [XmlElement("MST")]
        public string MST { get; set; }

        #region dùng cho 03XKNB, 04HGDL

        /// <summary>
        /// Lệnh điều động nội bộ
        /// Tối đa 255 ký tự
        /// Bắt buộc
        /// </summary>
        [XmlElement("LDDNBo")]
        public string LDDNBo { get; set; }

        /// <summary>
        /// Tên người vận chuyển
        /// Tối đa 400 ký tự
        /// Bắt buộc
        /// </summary>
        [XmlElement("TNVChuyen")]
        public string TNVChuyen { get; set; }

        /// <summary>
        /// Phương tiện vận chuyển
        /// Tối đa 50 ký tự
        /// Bắt buộc
        /// </summary>
        [XmlElement("PTVChuyen")]
        public string PTVChuyen { get; set; }

        #endregion

        /// <summary>
        /// Địa chỉ người bán
        /// Tối đa 400 ký tự
        /// Bắt buộc
        /// </summary>
        [XmlElement("DChi")]
        public string DChi { get; set; }


        /// <summary>
        /// chứa thông tin khác (Nếu có)
        /// </summary>
        [XmlElement("TTKhac")]
        public TTKhac TTKhac { get; set; }
    }

    [XmlType("NMua")]
    public class NDHDonNMua
    {
        /// <summary>
        /// Tên người bán
        /// Tối đa 400 ký tự
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// </summary>
        [XmlElement("Ten")]
        public string Ten { get; set; }

        /// <summary>
        /// Mã số thuế người bán
        /// Tối đa 14 ký tự
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// </summary>
        [XmlElement("MST")]
        public string MST { get; set; }

        /// <summary>
        /// Địa chỉ người bán
        /// Tối đa 400 ký tự
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// </summary>
        [XmlElement("DChi")]
        public string DChi { get; set; }

        /// <summary>
        /// chứa thông tin khác (Nếu có)
        /// </summary>
        [XmlElement("TTKhac")]
        public TTKhac TTKhac { get; set; }
    }


    [XmlType("DSHHDVu")]
    public class DSHHDVu
    {
        /// <summary>
        /// Mỗi 1 phần tử chứa chi tiết 01 dòng hàng hóa dịch vụ
        /// </summary>
        [XmlElement("HHDVu")]
        public List<HHDVu> HHDVu { get; set; }
    }

    public class HHDVu
    {
        /// <summary>
        /// Tính chất. Giá trị như sau:
        /// 1 : Hàng hóa, dịch vụ
        /// 2 : Khuyến mại
        /// 3 : Chiết khấu thương mại(trong trường hợp muốn thể hiện thông tin chiết khấu theo dòng)
        /// 4 : Ghi chú, diễn giải
        /// KHÔNG bắt buộc
        /// </summary>
        [XmlElement("TChat")]
        public int TChat { get; set; }

        /// <summary>
        /// Số thứ tự
        /// Dài tối đa 4 ký tự
        /// KHÔNG bắt buộc
        /// </summary>
        [XmlElement("STT")]
        public int STT { get; set; }

        /// <summary>
        /// Tên hàng hóa
        /// Dài tối đa 500 ký tự
        /// Bắt buộc
        /// </summary>
        [XmlElement("Ten")]
        public string Ten { get; set; }

        /// <summary>
        /// Đơn vị tính
        /// Dài tối đa 50 ký tự
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// </summary>
        [XmlElement("DVTinh")]
        public string DVTinh { get; set; }

        /// <summary>
        /// Số lượng
        /// Dài tối đa 19,4 ký tự. Tức là dài tối đa 19 ký tự trong đó tối đa 4 ký tự ở phần thập phân
        /// Sử dụng dấu chấm (.) để phân tách phần nguyên và phần thập phân (nếu có)
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// </summary>
        [XmlElement("SLuong")]
        public double SLuong { get; set; }

        /// <summary>
        /// Đơn giá
        /// Dài tối đa 19,4 ký tự. Tức là dài tối đa 19 ký tự trong đó tối đa 4 ký tự ở phần thập phân
        /// Sử dụng dấu chấm (.) để phân tách phần nguyên và phần thập phân (nếu có)
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// </summary>
        [XmlElement("DGia")]
        public decimal DGia { get; set; }

        #region dùng cho 01GTKT, 02GTTT

        /// <summary>
        /// Tỷ lệ % chiết khấu (Trong trường hợp muốn thể hiện thông tin chiết khấu theo cột cho từng hàng hóa, dịch vụ)
        /// Dài tối đa 6,4 ký tự. Tức là dài tối đa 6 ký tự trong đó tối đa 4 ký tự ở phần thập phân
        /// Sử dụng dấu chấm (.) để phân tách phần nguyên và phần thập phân (nếu có)
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// </summary>
        [XmlElement("TLCKhau")]
        public double TLCKhau { get; set; }

        /// <summary>
        /// Số tiền chiết khấu (Trong trường hợp muốn thể hiện thông tin chiết khấu theo cột cho từng hàng hóa, dịch vụ)
        /// Dài tối đa 19,4 ký tự. Tức là dài tối đa 19 ký tự trong đó tối đa 4 ký tự ở phần thập phân
        /// Sử dụng dấu chấm (.) để phân tách phần nguyên và phần thập phân (nếu có)
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// </summary>
        [XmlElement("STCKhau")]
        public decimal STCKhau { get; set; }
        #endregion

        /// <summary>
        /// Thành tiền (Thành tiền chưa có thuế GTGT)
        /// Dài tối đa 19,4 ký tự. Tức là dài tối đa 19 ký tự trong đó tối đa 4 ký tự ở phần thập phân
        /// Sử dụng dấu chấm (.) để phân tách phần nguyên và phần thập phân (nếu có)
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// </summary>
        [XmlElement("ThTien")]
        public decimal ThTien { get; set; }

        #region chỉ dùng cho 01GTKT
        /// <summary>
        /// Thuế suất (Thuế suất thuế GTGT)
        /// Dài tối đa 5 ký tự
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// </summary>
        [XmlElement("TSuat")]
        public string TSuat { get; set; }
        #endregion

        /// <summary>
        /// chứa thông tin khác
        /// </summary>
        [XmlElement("TTkhac")]
        public TTKhac TTkhac { get; set; }
    }

    /// <summary>
    /// chứa thông tin thanh toán của hóa đơn
    /// </summary>
    [XmlType("TToan")]
    public class TToan
    {
        #region chỉ dùng cho 01GTKT
        /// <summary>
        /// chứa thông tin tổng hợp theo từng loại thuế suất
        /// </summary>
        [XmlElement("THTTLTSuat")]
        public THTTLTSuat THTTLTSuat { get; set; }

        /// <summary>
        /// Tổng tiền chưa thuế (Tổng cộng thành tiền chưa có thuế GTGT) 
        /// Dài tối đa 19,4 ký tự. Tức là dài tối đa 19 ký tự trong đó tối đa 4 ký tự ở phần thập phân
        /// Sử dụng dấu chấm (.) để phân tách phần nguyên và phần thập phân (nếu có)
        /// Bắt buộc 
        /// </summary>
        [XmlElement("TgTCThue")]
        public decimal TgTCThue { get; set; }

        /// <summary>
        /// Tổng tiền thuế (Tổng cộng tiền thuế GTGT)
        /// Dài tối đa 19,4 ký tự. Tức là dài tối đa 19 ký tự trong đó tối đa 4 ký tự ở phần thập phân
        /// Sử dụng dấu chấm (.) để phân tách phần nguyên và phần thập phân (nếu có)
        /// Bắt buộc 
        /// </summary>
        [XmlElement("TgTThue")]
        public decimal TgTThue { get; set; }
        #endregion

        /// <summary>
        /// chứa danh sách các loại tiền phí, lệ phí (nếu có)
        /// </summary>
        [XmlElement("DSLPhi")]
        public DSLPhi DSLPhi { get; set; }

        /// <summary>
        /// Tổng tiền chiết khấu thương mại
        /// Dài tối đa 19,4 ký tự. Tức là dài tối đa 19 ký tự trong đó tối đa 4 ký tự ở phần thập phân
        /// Sử dụng dấu chấm (.) để phân tách phần nguyên và phần thập phân (nếu có)
        /// KHÔNG bắt buộc
        /// </summary>
        [XmlElement("TTCKTMai")]
        public decimal TTCKTMai { get; set; }

        /// <summary>
        /// Tổng tiền thanh toán bằng số
        /// Dài tối đa 19,4 ký tự. Tức là dài tối đa 19 ký tự trong đó tối đa 4 ký tự ở phần thập phân
        /// Sử dụng dấu chấm (.) để phân tách phần nguyên và phần thập phân (nếu có)
        /// bắt buộc
        /// </summary>
        [XmlElement("TgTTTBSo")]
        public decimal TgTTTBSo { get; set; }

        /// <summary>
        /// Tổng tiền thanh toán bằng chữ
        /// Dài tối đa 19,4 ký tự. Tức là dài tối đa 19 ký tự trong đó tối đa 4 ký tự ở phần thập phân
        /// Sử dụng dấu chấm (.) để phân tách phần nguyên và phần thập phân (nếu có)
        /// bắt buộc
        /// </summary>
        [XmlElement("TgTTTBChu")]
        public decimal TgTTTBChu { get; set; }

        /// <summary>
        /// chứa thông tin khác (nếu có)
        /// </summary>
        [XmlElement("TTKhac")]
        public TTKhac TTKhac { get; set; }
    }

    /// <summary>
    /// chứa thông tin tổng hợp theo từng loại thuế suất
    /// </summary>
    [XmlType("THTTLTSuat")]
    public class THTTLTSuat
    {
        /// <summary>
        /// chứa chi tiết thông tin tổng hợp của mỗi loại thuế suất
        /// </summary>
        [XmlElement("LTSuat")]
        public List<LTSuat> LTSuat { get; set; }
    }

    /// <summary>
    /// chứa chi tiết thông tin tổng hợp của mỗi loại thuế suất
    /// </summary>
    [XmlType("LTSuat")]
    public class LTSuat
    {
        /// <summary>
        /// Thuế suất (Thuế suất thuế GTGT)
        /// Dài tối đa 5 ký tự
        /// Bắt buộc
        /// </summary>
        [XmlElement("TSuat")]
        public string TSuat { get; set; }

        /// <summary>
        /// Thành tiền (Thành tiền chưa có thuế GTGT)
        /// Dài tối đa 19,4 ký tự
        /// Bắt buộc
        /// </summary>
        [XmlElement("ThTien")]
        public decimal ThTien { get; set; }

        /// <summary>
        /// Tiền thuế (Tiền thuế GTGT)
        /// Dài tối đa 19,4 ký tự
        /// Bắt buộc
        /// </summary>
        [XmlElement("TThue")]
        public decimal TThue { get; set; }
    }

    [XmlType("DSLPhi")]
    public class DSLPhi
    {
        /// <summary>
        /// chứa chi tiết từng loại tiền phí, lệ phí. Thẻ này có thể lặp lại nhiều lần tương ứng với số loại phí, lệ phí.
        /// </summary>
        [XmlElement("LPhi")]
        public LPhi LPhi { get; set; }
    }

    [XmlType("LPhi")]
    public class LPhi
    {
        /// <summary>
        /// Tên loại phí
        /// Dài tối đa 100 ký tự
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// </summary>
        [XmlElement("TLPhi")]
        public string TLPhi { get; set; }

        /// <summary>
        /// Tiền phí
        /// Dài tối đa 19,4 ký tự. Tức là dài tối đa 19 ký tự trong đó tối đa 4 ký tự ở phần thập phân
        /// Sử dụng dấu chấm (.) để phân tách phần nguyên và phần thập phân (nếu có)
        /// Bắt buộc (Trừ trường hợp quy định tại Điều 3, Thông tư số 68/2019/TT-BTC)
        /// </summary>
        [XmlElement("TPhi")]
        public decimal TPhi { get; set; }
    }

    /// <summary>
    /// Chứa các thông tin bổ sung của Người nộp thuế (nếu có).
    /// </summary>
    [XmlType("TTKhac")]
    public class TTKhac
    {
        [XmlElement("TTin")]
        public TTin TTin { get; set; }
    }

    [XmlType("TTin")]
    public class TTin
    {
        /// <summary>
        /// chứa tên trường thông tin cần hiển thị trên hóa đơn điện tử.
        /// Vd: muốn hiển thị Mã hàng hóa thì ghi là Mã hàng hóa
        /// Bắt buộc
        /// </summary>
        [XmlElement("TTruong")]
        public string TTruong { get; set; }

        /// <summary>
        /// chứa kiểu dữ liệu của thông tin cần hiển thị (string/numeric,…)
        /// Vd: Kiểu dữ liệu của Mã hàng hóa là string
        /// Bắt buộc
        /// </summary>
        [XmlElement("KDLieu")]
        public string KDLieu { get; set; }

        /// <summary>
        /// chứa dữ liệu cần hiển thị.
        /// Vd: Mã hàng hóa là SP001 thì thẻ có giá trị là SP001
        /// Bắt buộc
        /// </summary>
        [XmlElement("DLieu")]
        public string DLieu { get; set; }
    }

}
