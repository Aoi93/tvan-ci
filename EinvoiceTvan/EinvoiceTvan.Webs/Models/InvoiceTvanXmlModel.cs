﻿using System.Xml.Serialization;

namespace EinvoiceTvan.Webs.Models
{
    /// <summary>
    /// Thông điệp gửi hóa đơn điện tử tới cơ quan thuế để cấp mã
    /// Thẻ TDiep chứa thông tin truyền nhận bao gồm thông tin chung, thông tin chi tiết và thông tin chữ ký số
    /// Dung lượng tối đa của một thông điệp là 2MB.
    /// </summary>
    [XmlType("TDiep")]
    public class InvoiceTvanXmlModel
    {
        /// <summary>
        /// Chứa các thông tin phiên bản, mã nơi gửi, mã nơi nhận, mã loại thông điệp, mã thông điệp, mã thông điệp tham chiếu, mã số thuế, số lượng.
        /// </summary>
        [XmlElement("TTChung")]
        public TTChung TTChung { get; set; }

        /// <summary>
        /// Chứa các thông tin hóa đơn, thông báo hủy, đề nghị, bảng tổng hợp dữ liệu hóa đơn điện tử gửi cơ quan thuế,... được định nghĩa tại Phần II quy định này.
        /// Mỗi thông điệp chứa một loại dữ liệu của một NNT. Loại dữ liệu bao gồm: Dữ liệu hóa đơn điện tử có mã, hóa đơn điện tử không có mã và dữ liệu khác. Với loại dữ liệu hóa đơn điện tử có mã, mỗi thông điệp chỉ chứa dữ liệu của một hóa đơn.
        /// </summary>
        [XmlElement("DLieu")]
        public DLieu DLieu { get; set; }

        //TODO : khi có chữ ký số thì add thẻ này vào
        ///// <summary>
        ///// chữ ký số: chứa thông tin chữ ký số của bên gửi
        ///// </summary>
        //[XmlElement("Signature")]
        //public string Signature { get; set; }
    }

    /// <summary>
    /// dữ liệu
    /// Mỗi thông điệp chứa một loại dữ liệu của một NNT. 
    /// Loại dữ liệu bao gồm: Dữ liệu hóa đơn điện tử có mã, hóa đơn điện tử không có mã và dữ liệu khác. 
    /// Với loại dữ liệu hóa đơn điện tử có mã, mỗi thông điệp chỉ chứa dữ liệu của một hóa đơn.
    /// </summary>
    public class DLieu
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("HDon")]
        public HDon HDon { get; set; }
    }

}
