﻿using Infrastructure.Message.Rabbit;

namespace EinvoiceTvan.Webs.Service
{
    public class TvanBackgroundService : RabbitService, IRabbitService
    {
        public TvanBackgroundService(IRabbitBusClient busClient) : base(busClient)
        {
        }
    }
}
