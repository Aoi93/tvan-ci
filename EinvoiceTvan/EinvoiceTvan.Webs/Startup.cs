using EinvoiceTvan.Database;
using EinvoiceTvan.Database.SqlServer;
using EinvoiceTvan.Utils.DigitalSignature;
using EinvoiceTvan.Webs.Repository;
using Infrastructure.Database.Abstractions;
using Infrastructure.File.Minio;
using Jarvis.Core;
using Jarvis.Core.Database.SqlServer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace EinvoiceTvan.Webs
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Env { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.Configure<MinioOptions>(Configuration.GetSection("Minio"));
            services.AddMinio();

            //elatic-search
            
            services.AddElasticSearch(Configuration.GetSection("Elasticsearch"));


            services.AddScoped<IInvoiceRepository, InvoiceRepository>();

            services.AddCoreDbContext(Configuration);
            services.AddConfigCache(Configuration);
            services.AddConfigJson();
           

            //Jarvis
            services.AddConfigJarvisDefault<CoreDbContext>();
            services.AddConfigAuthentication();
            services.AddConfigAuthorization();

            AddDbContext(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHsts();
            app.UseHttpsRedirection();

            var configuration = app.ApplicationServices.GetService<IConfiguration>(); ;
            var jarvis = configuration.GetSection("Jarvis:RootFolder");
            app.UseConfigUI(jarvis.Value);

            app.UseRouting();

            app.UseCors(builder =>
            {
                builder.AllowAnyOrigin();
                builder.AllowAnyHeader();
                builder.AllowAnyMethod();
                builder.WithExposedHeaders("Content-Disposition");
            });

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseConfigSwagger();
            app.UseConfigMiddleware();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void AddDbContext(IServiceCollection services)
        {
            services.AddScoped<IStorageContext>(provider => provider.GetService<EinvoiceTvanDbContext>());
            services.AddDbContextPool<EinvoiceTvanDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("Core"), sqlOptions =>
                {
                    sqlOptions.MigrationsHistoryTable("__EinvoiceTvanMigrationHistory");
                });
            });
            services.AddScoped<IEinvoiceTvanUnitOfWork, EinvoiceTvanUnitOfWork>();
            services.AddScoped<IInvoiceRepository, InvoiceRepository>();
            services.AddScoped<IDigitalSignatureService, DigitalSignatureService>();
        }
    }
}
