﻿using EinvoiceTvan.Database;
using EinvoiceTvan.Database.Poco;
using EinvoiceTvan.Utils.DigitalSignature;
using EinvoiceTvan.Utils.Extensions;
using EinvoiceTvan.Webs.Enum;
using EinvoiceTvan.Webs.Models;
using EinvoiceTvan.Webs.Repository;
using Infrastructure.File.Abstractions;
using Infrastructure.Message.Rabbit;
using Jarvis.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace EinvoiceTvan.Webs.Controllers
{
    public interface IGetInvoiceController
    {
        /// <summary>
        /// api nhận 1 hóa đơn
        /// </summary>
        /// <param name="files"></param>
        /// <param name="IdTransaction"></param>
        Task<string> GetInvoiceAsync(IFormFile file, string idTransaction);
    }
    
    [Authorize]
    [Route("get")]
    [ApiController]
    public class GetInvoiceController : IGetInvoiceController
    {
        private readonly IEinvoiceTvanUnitOfWork _tvanUoW;
        private readonly IFileService _fileService;
        private readonly IWorkContext _workContext;
        private readonly IDigitalSignatureService _digitalSignatureService;
        private readonly IRabbitService _rabbitService;
        private readonly IConfiguration _configuration;

        public GetInvoiceController(IEinvoiceTvanUnitOfWork tvanUoW,
                                 IFileService fileService,
                                 IWorkContext workContext,
                                 IDigitalSignatureService digitalSignatureService,
                                 IRabbitService rabbitService,
                                 IConfiguration configuration
                                 )
        {
            _tvanUoW = tvanUoW;
            _fileService = fileService;
            _workContext = workContext;
            _digitalSignatureService = digitalSignatureService;
            _rabbitService = rabbitService;
            _configuration = configuration;
        }

        [Authorize]
        [HttpPost("invoice")]
        public async Task<string> GetInvoiceAsync([FromRoute]IFormFile file, [FromRoute]string idTransaction)
        {
            var invoiceRepository = _tvanUoW.GetRepository<IInvoiceRepository>();
            var invoices = await invoiceRepository.GetEnvoiceAsync(idTransaction);
            var fileName = file.FileName;

            if (invoices != null)
                return "Id Transaction này đã tồn tại!";
            
            using (var stream = new MemoryStream())
            {
                file.CopyTo(stream);
                if (stream.Length > 5242880)
                    return "Dung lượng file phải nhỏ hơn hoặc bằng 5MB";

                var result = new StringBuilder();

                //// 1. Check if IFormFile file contains something
                //// 2. Check if the file's extension is the one you are looking for (.dat)
                //// 3. Check if the file's Mime type is correct to avoid attacks
                /// explain by StackOverflow
                /// https://stackoverflow.com/questions/55850656/how-can-i-read-a-file-which-will-be-upload-from-a-form-in-net-core-api

                using (var reader = new StreamReader(file.OpenReadStream()))
                {
                    while (reader.Peek() >= 0)
                        result.AppendLine(await reader.ReadLineAsync());
                }
                var xml = result.ToString();

                var verifyXML = VerifyXml(xml);

                if (verifyXML == false)
                    return "File không hợp lệ!";

                var xmlTvanSigned = TvanSign(xml);

                var id = await UploadFileToMinio(idTransaction, fileName, xmlTvanSigned);

                _rabbitService.Publish(new InvoiceElasticInputModel
                {
                    IdTransaction = idTransaction,
                    XmlTvanSigned = xmlTvanSigned
                }, RabbitKey.Exchanges.Events, "SearchInvoice");

                return xmlTvanSigned;
            }
        }

        /// <summary>
        /// Verify Xml trước khi Tvan ký
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        private bool VerifyXml(string xml)
        {
            var verify = _digitalSignatureService.Verify(xml);
            foreach (var item in verify)
            {
                if (item.IsValid == false)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Tvan ký
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        private string TvanSign(string xml)
        {
            var signatureIdTvan = _configuration.GetSection("SignatureIdTvan:Id");
            var store = new X509Store(StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly);
            var certificates = store.Certificates;
            var xmlTvanSigned = _digitalSignatureService.Sign(certificates[0], xml, signatureIdTvan.Value);
            return xmlTvanSigned;
        }

        /// <summary>
        /// 1. Lưu thông tin file upload vào DB
        /// 2. Mã hóa và upload File lên Minio
        /// </summary>
        /// <param name="idTransaction"></param>
        /// <param name="fileName"></param>
        /// <param name="stream"></param>
        /// <param name="xml"></param>
        /// <returns></returns>
        private async Task<int> UploadFileToMinio(string idTransaction, string fileName, string xml)
        {
            var invoiceRepo = _tvanUoW.GetRepository<Infrastructure.Database.Abstractions.IRepository<Invoice>>();
            var tenantCode = await _workContext.GetTenantCodeAsync();
            var securityStemp = CrytographyExtension.AES_GenerateKey(256);

            var invoiceEntity = new Invoice
            {
                Code = Guid.NewGuid(),
                TenantCode = tenantCode,
                IdTransaction = idTransaction,
                Status = (int)StatusEnum.UnSend,
                CreatedAt = DateTime.Now,
                CreatedUtc = DateTime.UtcNow,
                SecurityStemp = securityStemp,
                FileName = fileName
            };

            byte[] key = Convert.FromBase64String(securityStemp);

            var now = DateTime.Now;
            var aes_ComputeIV = CrytographyExtension.AES_ComputeIV(key, key.Length / 2);
            var aes_Encrypt = CrytographyExtension.AES_Encrypt(xml, key, aes_ComputeIV);
            var filePathMinio = $"{tenantCode}/{now.Year}/{now.Month.ToString("D2")}/{now.Day.ToString("D2")}/{now.Hour.ToString("D2")}/{invoiceEntity.Code}" + ".xml";

            await _fileService.UploadAsync(filePathMinio, aes_Encrypt);
            await invoiceRepo.InsertAsync(invoiceEntity);
            await _tvanUoW.CommitAsync();

            return invoiceEntity.Id;
        }

        /// <summary>
        /// fake data theo mẫu của TCT
        /// </summary>
        /// <returns></returns>
        [HttpPost("create-fake-data")]
        public string CreateFakeData()
        {
            var invoiceTvanXmlWithoutCodeModel = new InvoiceTvanXmlWithoutCodeModel();
            var ttChung = new TTChung();

            ttChung.PBan = "1.0.0";
            ttChung.MNGui = "0101352495999";
            ttChung.MNNhan = "010581529611";
            ttChung.MLTDiep = 200;
            ttChung.MTDiep = "0101352495998210000000002";
            ttChung.MTDTChieu = "0105815296210000000002";
            ttChung.MST = "0101352495999";
            ttChung.SLuong = "888";

            var dLieuWithoutCode = new DLieuWithoutCode();

            var lstHoaDon = new List<HDon>();
            var hoaDon = new HDon();
           
            var dlHDon = new DLHDon();

            var ttChungDLHDon = new TTChungDLHDon();

            ttChungDLHDon.PBan = "1.0.0";
            ttChungDLHDon.THDon = "HÓA ĐƠN THANH TOÁN ĐỒ DÙNG CÔNG NGHỆ";
            ttChungDLHDon.KHMSHDon = 1;
            ttChungDLHDon.KHHDon = "AA/17H";
            ttChungDLHDon.SHDon = 22;
            ttChungDLHDon.TDLap = DateTime.Now;
            ttChungDLHDon.DVTTe = "VND";
            ttChungDLHDon.TGia = 1;

            var ttKhac = new TTKhac();
            var ttin = new TTin();
            ttin.TTruong = "mã hàng hóa";
            ttin.KDLieu = "string";
            ttin.DLieu = "SP002";
            ttKhac.TTin = ttin;
            ttChungDLHDon.TTKhac = ttKhac;
            dlHDon.TTChung = ttChungDLHDon;
            var nDHDon = new NDHDon();
            var nBan = new NDHDonNBan();
            nBan.Ten = "Cửa hàng điện tử Cầu Giấy";
            nBan.MST = "1235679876543";
            nBan.DChi = "Số 5 ngõ 102 Nguyễn Đình Hoàn, Phường Nghĩa Đô, Quận Cầu Giấy, Thành phố Hà Nội";
            nDHDon.NBan = nBan;

            var nMua = new NDHDonNMua();
            nMua.Ten = "Nguyễn Hữu Đức";
            nMua.MST = "123567983456";
            nMua.DChi = "Số 6 ngõ 103 Nguyễn Đình Hoàn, Phường Nghĩa Đô, Quận Cầu Giấy, Thành phố Hà Nội";

            nDHDon.NMua = nMua;

            var dsHHDVu = new DSHHDVu();
            var lstHHDVu = new List<HHDVu>();
            var hhDVu = new HHDVu();
            hhDVu.TChat = 1;
            hhDVu.STT = 1;
            hhDVu.Ten = "Đồng hồ thông minh";
            hhDVu.DVTinh = "Chiếc";
            hhDVu.SLuong = 5;
            hhDVu.DGia = 2000000;
            hhDVu.TLCKhau = 0.5;
            hhDVu.STCKhau = 15000;
            hhDVu.ThTien = 10000000;
            hhDVu.TSuat = "1";

            //-----
            var hhDVu1 = new HHDVu();
            hhDVu1.TChat = 1;
            hhDVu1.STT = 1;
            hhDVu1.Ten = "Iphone 11 pro max";
            hhDVu1.DVTinh = "Chiếc";
            hhDVu1.SLuong = 10;
            hhDVu1.DGia = 34000000;
            hhDVu1.TLCKhau = 0.5;
            hhDVu1.STCKhau = 250000;
            hhDVu1.ThTien = 340000000;
            hhDVu1.TSuat = "1";
            lstHHDVu.Add(hhDVu);
            lstHHDVu.Add(hhDVu1);
            dsHHDVu.HHDVu = lstHHDVu;
            nDHDon.DSHHDVu = dsHHDVu;

            var ttToan = new TToan();
            var thTTLTSuat = new THTTLTSuat();
            var lstLTSuat = new List<LTSuat>();
            var ltSuat = new LTSuat();
            ltSuat.TSuat = "5";
            ltSuat.ThTien = 44000000;
            ltSuat.TThue = 50000;
            lstLTSuat.Add(ltSuat);
            thTTLTSuat.LTSuat = lstLTSuat;

            ttToan.THTTLTSuat = thTTLTSuat;
            ttToan.TgTCThue = 44000000;
            ttToan.TgTThue = 500000;
            ttToan.TTCKTMai = 1000000;
            ttToan.TgTTTBSo = 43000000;
            ttToan.TgTTTBChu = 43000000;

            nDHDon.TToan = ttToan;

            dlHDon.NDHDon = nDHDon;

            //dlHDon.DLQRCode = "qwsedrftgyuhijol8761234567890esdkjertyfcgmsdfumnbfghjoiuytre";

            hoaDon.DLHDon = dlHDon;
            //hoaDon.MCCQT = "qwertyuiopghjklkjhgfdio";

            lstHoaDon.Add(hoaDon);

            dLieuWithoutCode.HDon = lstHoaDon;
            invoiceTvanXmlWithoutCodeModel.TTChung = ttChung;
            invoiceTvanXmlWithoutCodeModel.DLieu = dLieuWithoutCode;


            using (var stringwriter = new StringWriter())
            {
                var serializer = new XmlSerializer(invoiceTvanXmlWithoutCodeModel.GetType());
                serializer.Serialize(stringwriter, invoiceTvanXmlWithoutCodeModel);
                var xml = stringwriter.ToString();

                var store = new X509Store(StoreLocation.CurrentUser);

                store.Open(OpenFlags.ReadOnly);

                var certificates = store.Certificates;

                var count = certificates.Count;
                return stringwriter.ToString();
            }
            //XmlDocument doc = new XmlDocument();
            //doc.LoadXml(content);
            //XmlNodeList elemList = doc.GetElementsByTagName("invoiceType");
            //if (elemList.Count > 0)
            //{
            //    invoiceType = elemList[0].InnerXml;
            //}
            //else
            //{
            //    invoiceType = "OtherInvoiceType";
            //}

            // convert data to model 
            //XmlNodeList signatureTag = doc.GetElementsByTagName("X509SubjectName");
            //string x509SubjectName = "";
            //if (signatureTag.Count > 0)
            //{
            //    x509SubjectName = signatureTag[0].InnerXml;
            //}
        }
    }
}
