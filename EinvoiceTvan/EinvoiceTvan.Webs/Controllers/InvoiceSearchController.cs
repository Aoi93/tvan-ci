﻿using EinvoiceTvan.Utils.DigitalSignature;
using EinvoiceTvan.Webs.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nest;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace EinvoiceTvan.Webs.Controllers
{
    [Route("portal")]
    [ApiController]
    public class InvoiceSearchController : ControllerBase
    {
        private readonly IElasticClient _elasticClient;
        private readonly IDigitalSignatureService _digitalSignatureService;

        public InvoiceSearchController(IElasticClient elasticClient,
            IDigitalSignatureService digitalSignatureService)
        {
            _digitalSignatureService = digitalSignatureService;
            _elasticClient = elasticClient;
        }

        [HttpPost("invoice-search")]
        public async Task<InvoiceTvanElasticModel> Post([FromBody] InvoiceSearchModel input)
        {
            #region search in elastic
            var searchRequest = new SearchRequest<InvoiceTvanElasticModel>(Indices.All)
            {
                From = 0,
                Size = 1,
                Query = new MatchQuery
                {
                    Field = Infer.Field<invoice>(f => f.IdTransaction),
                    Query = input.IdTransaction
                }
            };

            var searchResponse = await _elasticClient.SearchAsync<InvoiceTvanElasticModel>(searchRequest);

            if (searchResponse.Documents.Count <= 0)
                return null;

            var items = searchResponse.Documents;
            var data = new InvoiceTvanElasticModel();
            foreach (var item in searchResponse.Documents)
            {
                data = item;
            }

            return data;
            #endregion

            #region seach in DB
            // search in DB
            //var invoices = await invoiceRepository.GetEnvoiceAsync(idTransaction);

            //if (invoices != null)
            //{
            //    var tenantCode = invoices.TenantCode;
            //    var createAt = invoices.CreatedAt;

            //    var filePath = $"/01GTKT/{tenantCode}/{createAt.Year}/{createAt.Month.ToString("D2")}/{createAt.Day.ToString("D2")}/{createAt.Hour.ToString("D2")}/{invoices.Code}" + ".xml";

            //    byte[] files = await _fileService.DownloadAsync(filePath);

            //    byte[] key = Convert.FromBase64String(invoices.SecurityStemp);

            //    var aes_ComputeIV = CrytographyExtension.AES_ComputeIV(key, key.Length / 2);

            //    var aes_Decrypt = CrytographyExtension.AES_Decrypt(files, key, aes_ComputeIV);
            //    XmlSerializer serializer = new XmlSerializer(typeof(invoice));
            //    StringReader reader = new StringReader(aes_Decrypt);
            //    invoice invoiceData = (invoice)serializer.Deserialize(reader);

            //    return invoiceData;
            //}
            //else
            //{
            //    return null;
            //}
            #endregion
        }

        [HttpPost("verify")]
        public async Task<List<CertificateModel>> Verify(IFormFile file)
        { 
            using (var stream = new MemoryStream())
            {
                file.CopyTo(stream);

                var result = new StringBuilder();

                using (var reader = new StreamReader(file.OpenReadStream()))
                {
                    while (reader.Peek() >= 0)
                        result.AppendLine(await reader.ReadLineAsync());
                }

                var xml = result.ToString();

                var verify = _digitalSignatureService.Verify(xml);

                return verify;
            }
        }
    }
}
