(function () {
    'use strict';

    angular
        .module('jarvis')
        .constant('APP_CONFIG', {
            VISUALIZER: false,
            THEME: 'skin-blue',
            BASE_UI_PATH: '/app/',
            APP_NAME: 'Tvan',
            NAMESPACE_STORAGE: 'tvan',
            DEFAULT_URL: '/portal',
            DASHBOARD_URL: '/dashboard',
            LOGIN_URL: '/login',
            //Customize template of component
            TEMPLATE_URLS: {
                // 'uiCore': '/customize/core/core.template.html',
                // 'uiNavbar': '/customize/shared/components/navbar/navbar.template.html',
                // 'uiTopbar': '/customize/shared/components/topbar/topbar.template.html',
                // 'uiFooter': '/customize/shared/components/footer/footer.template.html',
                // 'uiSidebar': '/customize/shared/components/sidebar/sidebar.template.html',
                // 'uiUserInfo': '/app/shared/components/user-info/user-info.template.html',
                // 'uiIdentity': '/customize/identity/identity.template.html',
                // 'uiLogin': '/customize/identity/login/login.template.html',
                // 'uiUserCreate': '/customize/identity/users/user-create.template.html',
                // 'uiUserUpdate': '/customize/identity/users/user-update.template.html',
            },
            //Customize controller of component
            CONTROLLER_URLS: {
                // 'uiUserCreate': '/customize/identity/users/user-create.controller.js',
                // 'uiUserUpdate': '/customize/identity/users/user-update.controller.js',
            },
            //Mapping nagication code from BE to URL FE
            NAVIGATION_CODE_MAPPING_TO_STATE: {
                //Core
                'dashboard': 'dashboard',
                'system': null,
                'users': 'identity.backend.user',
                'roles': 'identity.backend.role',
                'labels': 'core.label',
                'settings': 'core.settings',
                'tenant-info': 'core.tenant-info',
                'tenants': 'core.tenant',
                'tenant': 'core.tenant',
                'licenses': 'core.license',
                'unit': 'catalog.unit',
                'currency': 'catalog.currency',
                'group-customer': 'catalog.customer-group',
                'customer': 'catalog.customer',
                'product-type': 'catalog.product-type',
                'product': 'catalog.product',
                //'tax': 'tax',
                'organizations': 'core.organization',
                'email-template': 'catalog.email-template',
                'email': 'catalog.email'

            },
            claimOfTenantAdmins: [
                'User_Read', 'User_Create', 'User_Update', 'User_Delete', 'User_Lock', 'User_Reset_Password',
                'Tenant_Create', 'Tenant_Read', 'Tenant_Update', 'Tenant_Delete',
                'Role_Create', 'Role_Delete', 'Role_Read', 'Role_Update',
                'Setting_Read', 'Setting_Update',
                'OrganizationUnit_Create', 'OrganizationUnit_Delete', 'OrganizationUnit_Read', 'OrganizationUnit_Roles', 'OrganizationUnit_Update', 'OrganizationUnit_Users',
                'Label_Create', 'Label_Delete', 'Label_Read', 'Label_Update',
                'License_Update'
            ],
            // Thêm thông báo vào cặp dấu ''
            MESSAGE: ''
        })
        .constant('CONST_PERMISSION', {
            Dashboard_Read: 'Dashboard_Read',

            // hệ thống
            User_Read: 'User_Read',
            User_Create: 'User_Create',
            User_Update: 'User_Update',
            User_Delete: 'User_Delete',
            User_Lock: 'User_Lock',
            User_Reset_Password: 'User_Reset_Password',
            Role_Read: 'Role_Read',
            Role_Create: 'Role_Create',
            Role_Update: 'Role_Update',
            Role_Delete: 'Role_Delete',
            Tenant_Read: 'Tenant_Read',
            Tenant_Create: 'Tenant_Create',
            Tenant_Update: 'Tenant_Update',
            Tenant_Delete: 'Tenant_Delete',
            Label_Read: 'Label_Read',
            Label_Create: 'Label_Create',
            Label_Update: 'Label_Update',
            Label_Delete: 'Label_Delete',
            Setting_Read: 'Setting_Read',
            Setting_Update: 'Setting_Update',
            License_Update: 'License_Update',
            OrganizationUnit_Read: 'OrganizationUnit_Read',
            OrganizationUnit_Create: 'OrganizationUnit_Create',
            OrganizationUnit_Update: 'OrganizationUnit_Update',
            OrganizationUnit_Delete: 'OrganizationUnit_Delete',
            OrganizationUnit_Users: 'OrganizationUnit_Users',
            OrganizationUnit_Roles: 'OrganizationUnit_Roles',
        });
})();