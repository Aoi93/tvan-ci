(function () {
    'use strict';

    function portalController($state, $timeout, httpService, sweetAlert, $window, cacheService, APP_CONFIG, FileUploader, portalService, $location, $scope, $anchorScroll) {
        var ctrl = this;

        ctrl.uploader = new FileUploader({
            url: '/portal/verify',
            queueLimit: 1,
            autoUpload: true,
            removeAfterUpload: true
        });

        var types = ['text/xml'];

        ctrl.transaction = {};
        ctrl.tabActive = 'home';
        ctrl.xmlData = {};
        ctrl.isShow = false;
        ctrl.loading = false;
        ctrl.isSearch = false;
        ctrl.isValid = false;
        ctrl.verificationCode = null;
        ctrl.version = null;
        ctrl.messageType = null;
        ctrl.certificates = [];

        ctrl.$onInit = function () {
            ctrl.uploader.success = 0;
            ctrl.tabActive = 'home';
        };

        ctrl.uploader.filters.push({
            name: 'extensions',
            fn: function (item, options) {
                if (types.indexOf(item.type) === -1) {
                    return false;
                }
                return true;
            }
        });

        ctrl.search = function () {
            const idTransaction = ctrl.transaction;
            portalService.post(idTransaction).then(function (response) {
                if (response.data) {
                    ctrl.xmlData = response.data;
                    $location.hash('invoiceInfo');
                    $anchorScroll();
                } else {
                    sweetAlert.swal({
                        title: "Lỗi",
                        text: "Không tìm thấy dữ liệu hợp lệ!",
                        type: "error",
                        timer: 2000
                    });
                }
            })
        };

        ctrl.searchTab = function () {
            ctrl.tabActive = 'search';
            ctrl.isSearch = true;
        }

        ctrl.verify = function () {
            ctrl.tabActive = 'verify';
            ctrl.isSearch = false;
        }

        ctrl.home = function () {
            ctrl.tabActive = 'home';
        }

        ctrl.validate = function () {
            for (let i = 0; i < ctrl.certificates.length; i++) {
                const element = ctrl.certificates[i];
                if (!element.isValid) {
                    ctrl.isValid = false;
                    return;
                }
            }
            ctrl.isValid = true;
        };

        ctrl.uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };

        // ctrl.uploader.onAfterAddingFile = function(fileItem) {
        //     console.info('onAfterAddingFile', fileItem);
        // };

        // ctrl.uploader.onAfterAddingAll = function(addedFileItems) {
        //     console.info('onAfterAddingAll', addedFileItems);
        // };

        // ctrl.uploader.onBeforeUploadItem = function(item) {
        //     console.info('onBeforeUploadItem', item);
        // };

        // ctrl.uploader.onProgressItem = function(fileItem, progress) {
        //     console.info('onProgressItem', fileItem, progress);
        // };

        // ctrl.uploader.onProgressAll = function(progress) {
        //     console.info('onProgressAll', progress);
        // };

         ctrl.uploader.onSuccessItem = function(fileItem, response, status, headers) {
             console.info('onSuccessItem', fileItem, response, status, headers);
         };

        ctrl.uploader.onErrorItem = function (fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);

            sweetAlert.swal({
                title: "Lỗi",
                text: response,
                type: "error",
            });
        };

        ctrl.uploader.onCancelItem = function (fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };

        ctrl.uploader.onCompleteItem = function (fileItem, response, status, headers) {
            ctrl.uploader.success += 1;
            if (response.length > 0) {
                ctrl.certificates = response;
                ctrl.validate();
                ctrl.verificationCode = response.verificationCode;
                ctrl.version = response.version;
                ctrl.messageType = response.messageType;
                sweetAlert.swal({
                    title: "Thành công",
                    text: "Xác thực thành công",
                    type: "success",
                    timer: 1000
                });
                console.log("response", response);
            }
            else {
                sweetAlert.swal({
                    title: "Lỗi",
                    text: "Xác thực không thành công",
                    type: "error",
                    timer: 2000
                });
            }
        };

        // ctrl.uploader.onTimeoutItem = function(fileItem) {
        //     console.info('onTimeoutItem', fileItem);
        // };

        ctrl.uploader.onCompleteAll = function () {
             //sweetAlert.swal({
             //    title: "Thành công",
             //    text: "Tải lên tài liệu thành công",
             //    type: "success",
             //});
        };
    };

    angular
        .module('portal')
        .controller('portalController', portalController);

    portalController.$inject = ['$state', '$timeout', 'httpService', 'sweetAlert', '$window',
        'cacheService', 'APP_CONFIG', 'FileUploader', 'portalService',
        '$location', '$scope', '$anchorScroll'];
}());