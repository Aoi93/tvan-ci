(function () {
    'use strict';

    angular
        .module('portal', [])
        .component('uiPortal', {
            templateUrl: '/portal/portal.template.html',
            controller: 'portalController',
            bindings: {
                context: '='
            }
        })
        .config(function ($stateProvider) {
            $stateProvider.state('portal', {
                component: 'uiPortal',
                url: '/portal',
                resolve: {
                    uiBootstrap: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('moduleUiBootstrap');
                    }],
                    validate: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('moduleValidate');
                    }],
                    autofocus: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('moduleAutofocus');
                    }],
                    tooltip: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('moduleTooltip');
                    }],
                    moduleFileUpload: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('moduleFileUpload');
                    }],
                    portalController: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('/portal/portal.controller.js');
                    }],
                    portalService: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('/portal/portal.service.js');
                    }]
                    // chart: ['$ocLazyLoad', function ($ocLazyLoad) {
                    //     return $ocLazyLoad.load('moduleChart');
                    // }],
                    // daterangepicker: ['$ocLazyLoad', function ($ocLazyLoad) {
                    //     return $ocLazyLoad.load('moduleDaterangepicker');
                    // }],
                    // selectize: ['$ocLazyLoad', function ($ocLazyLoad) {
                    //     return $ocLazyLoad.load(['/libs/jquery/selectize/selectize.css', '/libs/jquery/selectize/selectize.js', '/libs/angular/angular-selectize.js']);
                    // }]
                }
            });

            //$stateProvider.state('portal.tvan-search', {
            //    url: '/portal/tvan-search',
            //    component: 'uiTvanSearch',
            //    resolve: {
            //        tvanSearchController: ['$ocLazyLoad', 'componentService', function ($ocLazyLoad, componentService) {
            //            return $ocLazyLoad.load(componentService.getControllerUrl('uiTvanSearch', '/portal/tvan-search/tvan-search.controller.js'));
            //        }]
            //    }
            //});
        })
        .config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
            $ocLazyLoadProvider.config({
                modules: [
                    {
                        name: 'moduleDaterangepicker',
                        serie: true,
                        files: [
                            'libs/jquery/daterangepicker/daterangepicker.css',
                            'libs/jquery/daterangepicker/moment-with-locales.js',
                            'libs/jquery/daterangepicker/daterangepicker.js',
                            'libs/angular/daterangepicker/angular-daterangepicker-init.js',
                            'libs/angular/daterangepicker/angular-daterangepicker-config.js',
                            'libs/angular/daterangepicker/angular-daterangepicker.js'
                        ]
                    },
                    {
                        name: 'moduleChart',
                        serie: true,
                        files: [
                            'libs/jquery/chart.js',
                            'libs/angular/angular-chart.js'
                        ]
                    },
                    {
                        name: 'moduleUiBootstrap',
                        serie: true,
                        files: [
                            'libs/jquery/moment.js',
                            'libs/jquery/moment-with-locales.js',
                            'libs/angular/ui-bootstrap-tpls-2-5-0.js'
                        ]
                    },
                    {
                        name: 'moduleFileUpload',
                        serie: true,
                        files: [
                            'libs/angular/angular-file-upload/angular-file-upload.css',
                            'libs/angular/angular-file-upload/angular-file-upload.js'
                        ]
                    }
                ]
            });
        }]);
}());