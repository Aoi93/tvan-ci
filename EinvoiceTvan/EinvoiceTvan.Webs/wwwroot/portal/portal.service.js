﻿(function () {
    'use strict';

    function portalService(httpService) {
        var api = '/portal';

        this.post = function (idTransaction) {
            return httpService.post(api + "/invoice-search", idTransaction);
        }
    }

    angular
        .module('portal')
        .service('portalService', portalService);
    portalService.$inject = ['httpService'];
})();
