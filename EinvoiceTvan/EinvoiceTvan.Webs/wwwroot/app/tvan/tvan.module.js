(function () {
    'use strict';

    angular
        .module('tvan', [
        ])
        .component('uiTvan', {
            templateUrl: '/app/tvan/tvan.template.html',
            controller: 'tvanController',
            bindings: {
                context: '='
            }
        })
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider.state('tvan', {
                abstract: true,
                component: 'uiTvan',
                resolve: {
                    uiBootstrap: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('moduleUiBootstrap');
                    }],
                    validate: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('moduleValidate');
                    }],
                    autofocus: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('moduleAutofocus');
                    }],
                    tooltip: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('moduleTooltip');
                    }]
                }
            });
        }])
        .config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
            $ocLazyLoadProvider.config({
                modules: [
                    {
                        name: 'moduleDaterangepicker',
                        serie: true,
                        files: [
                            'libs/jquery/daterangepicker/daterangepicker.css',
                            'libs/jquery/daterangepicker/moment-with-locales.js',
                            'libs/jquery/daterangepicker/daterangepicker.js',
                            'libs/angular/daterangepicker/angular-daterangepicker-init.js',
                            'libs/angular/daterangepicker/angular-daterangepicker-config.js',
                            'libs/angular/daterangepicker/angular-daterangepicker.js'
                        ]
                    },
                    {
                        name: 'moduleChart',
                        serie: true,
                        files: [
                            'libs/jquery/chart.js',
                            'libs/angular/angular-chart.js'
                        ]
                    },
                    {
                        name: 'moduleUiBootstrap',
                        serie: true,
                        files: [
                            'libs/jquery/moment.js',
                            'libs/jquery/moment-with-locales.js',
                            'libs/angular/ui-bootstrap-tpls-2-5-0.js'
                        ]
                    }
                ]
            });
        }])
        .controller('tvanController', ['$scope', 'APP_CONFIG', 'cacheService', function ($scope, APP_CONFIG, cacheService) {
            var ctrl = $scope.$ctrl;

            ctrl.$onInit = function () {
                var context = cacheService.get('context');
                ctrl.context = context;
                ctrl.context.theme = APP_CONFIG.THEME;
            };
        }]);
})();