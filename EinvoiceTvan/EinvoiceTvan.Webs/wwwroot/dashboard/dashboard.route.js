(function () {
    'use strict';

    angular
        .module('jarvis')
        .component('uiDashboard', {
            templateUrl: '/app/dashboard/dashboard.template.html',
            controller: 'dashboardController',
            bindings: {
                context: '='
            }
        })
        .config(function ($stateProvider) {
            $stateProvider.state('dashboard', {
                component: 'uiDashboard',
                url: '/dashboard',
                resolve: {
                    uiBootstrap: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('moduleUiBootstrap');
                    }],
                    validate: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('moduleValidate');
                    }],
                    autofocus: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('moduleAutofocus');
                    }],
                    tooltip: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('moduleTooltip');
                    }],
                    dashboardController: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('/app/dashboard/dashboard.controller.js');
                    }]
                    // chart: ['$ocLazyLoad', function ($ocLazyLoad) {
                    //     return $ocLazyLoad.load('moduleChart');
                    // }],
                    // daterangepicker: ['$ocLazyLoad', function ($ocLazyLoad) {
                    //     return $ocLazyLoad.load('moduleDaterangepicker');
                    // }],
                    // selectize: ['$ocLazyLoad', function ($ocLazyLoad) {
                    //     return $ocLazyLoad.load(['/libs/jquery/selectize/selectize.css', '/libs/jquery/selectize/selectize.js', '/libs/angular/angular-selectize.js']);
                    // }]
                }
            });
        })
        .config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
            $ocLazyLoadProvider.config({
                modules: [
                    {
                        name: 'moduleDaterangepicker',
                        serie: true,
                        files: [
                            'libs/jquery/daterangepicker/daterangepicker.css',
                            'libs/jquery/daterangepicker/moment-with-locales.js',
                            'libs/jquery/daterangepicker/daterangepicker.js',
                            'libs/angular/daterangepicker/angular-daterangepicker-init.js',
                            'libs/angular/daterangepicker/angular-daterangepicker-config.js',
                            'libs/angular/daterangepicker/angular-daterangepicker.js'
                        ]
                    },
                    {
                        name: 'moduleChart',
                        serie: true,
                        files: [
                            'libs/jquery/chart.js',
                            'libs/angular/angular-chart.js'
                        ]
                    },
                    {
                        name: 'moduleUiBootstrap',
                        serie: true,
                        files: [
                            'libs/jquery/moment.js',
                            'libs/jquery/moment-with-locales.js',
                            'libs/angular/ui-bootstrap-tpls-2-5-0.js'
                        ]
                    }
                ]
            });
        }]);
    // .controller('dashboardController', ['$scope', 'APP_CONFIG', 'cacheService', function ($scope, APP_CONFIG, cacheService) {
    //     var ctrl = $scope.$ctrl;

    //     ctrl.$onInit = function () {
    //         var context = cacheService.get('context');
    //         ctrl.context = context;
    //         ctrl.context.theme = APP_CONFIG.THEME;
    //     };
    // }]);
}());