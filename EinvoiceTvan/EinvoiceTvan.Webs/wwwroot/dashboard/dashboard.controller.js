(function () {
    'use strict';

    function dashboardController($state, $timeout, httpService, sweetAlert, $window, cacheService, APP_CONFIG) {
        var ctrl = this;

        ctrl.$onInit = function () {
            var context = cacheService.get('context');
            ctrl.context = context;
            ctrl.context.theme = APP_CONFIG.THEME;
        };
    };

    angular
        .module('jarvis')
        .controller('dashboardController', dashboardController);

    dashboardController.$inject = ['$state', '$timeout', 'httpService', 'sweetAlert', '$window', 'cacheService', 'APP_CONFIG'];
}());