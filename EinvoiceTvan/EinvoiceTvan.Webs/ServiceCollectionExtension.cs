using EinvoiceTvan.Database.SqlServer;
using EinvoiceTvan.Webs.Repository;
using Infrastructure;
using Infrastructure.Caching;
using Infrastructure.Caching.Redis;
using Infrastructure.Database.Abstractions;
using Infrastructure.File.Minio;
using Jarvis.Core.Database;
using Jarvis.Core.Database.SqlServer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nest;
using System;

namespace EinvoiceTvan
{
    public static class ServiceCollectionExtension
    {
        public static void AddCoreDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IStorageContext>(provider => provider.GetService<CoreDbContext>());
            services.AddDbContextPool<CoreDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("Core"), sqlOptions =>
                {
                    sqlOptions.MigrationsHistoryTable("__CoreMigrationHistory");
                });
            });
            services.AddScoped<ICoreUnitOfWork, CoreUnitOfWork>();
        }

        public static void AddConfigCache(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddStackExchangeRedisCache(options =>
            {
                options.InstanceName = configuration.GetSection("Redis:InstanceName").Value;
                options.ConfigurationOptions = new StackExchange.Redis.ConfigurationOptions
                {
                    EndPoints =
                    {
                        configuration.GetSection("Redis:EndPoint:Host").Value
                    },
                    Password = configuration.GetSection("Redis:Password").Value,
                    ConnectRetry = int.Parse(configuration.GetSection("Redis:ConnectRetry").Value),
                    AbortOnConnectFail = bool.Parse(configuration.GetSection("Redis:AbortOnConnectFail").Value),
                    ConnectTimeout = int.Parse(configuration.GetSection("Redis:ConnectTimeout").Value),
                    SyncTimeout = int.Parse(configuration.GetSection("Redis:SyncTimeout").Value),
                    DefaultDatabase = int.Parse(configuration.GetSection("Redis:DefaultDatabase").Value)
                };
            });
            services.AddSingleton<ICacheService, RedisCacheService>();
            services.Configure<DistributedCacheEntryOptions>(options =>
            {
                options.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(15);
            });
        }

        public static void AddConfigJson(this IServiceCollection services)
        {
            services.Configure<JsonOptions>(options =>
            {
                options.JsonSerializerOptions.IgnoreNullValues = true;

                //netcore2.2
                //options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                //options.SerializerSettings.ContractResolver = new DefaultContractResolver()
                //{
                //    NamingStrategy = new CamelCaseNamingStrategy()
                //};
            });
        }

        // public static void AddEvent(this IServiceCollection services)
        // {
        //     services.AddScoped<ITenantCreatedEvent, SendMailRootUserEvent>();
        //     services.AddScoped<ITenantCreatedEvent, SendMailUserAutoGenPasswordEvent>();

        //     services.AddScoped<IIdentityPasswordForgotedEvent, SendMailForgotPasswordEvent>();
        //     services.AddScoped<IUserPasswordResetedEvent, SendMailResetPasswordEvent>();
        //     services.AddScoped<IUserCreatedEvent, SendMailCreatedUserEvent>();
        // }

        public static void AddMinio(this IServiceCollection services)
        {
            services.AddSingleton<Infrastructure.File.Abstractions.IFileService, MinioService>((serviceProvider) =>
            {
                var options = serviceProvider.GetService<IOptions<MinioOptions>>();
                var logger = serviceProvider.GetService<ILogger<MinioService>>();
                return new MinioService(
                    minioOptions: options,
                    minio: new Minio.MinioClient(
                        options.Value.Endpoint,
                        options.Value.AccessKey,
                        options.Value.SecretKey,
                        options.Value.Region
                    ),
                    logger: logger
                );
            });
        }

        public static void AddElasticSearch(this IServiceCollection services, IConfiguration configuration)
        {
            var url = configuration["Url"];
            var defaultIndex = configuration["Index"];
            var userName = configuration["Username"];
            var passWord = configuration["Password"];
            var settings = new ConnectionSettings(new Uri(url)).DefaultIndex(defaultIndex);
            settings.EnableHttpCompression();
            settings.BasicAuthentication(userName, passWord);
            var client = new ElasticClient(settings);
            services.AddSingleton<IElasticClient>(client);
        }
    }
}
