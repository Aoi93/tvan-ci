﻿using Infrastructure.Database.Abstractions;
using System;

namespace EinvoiceTvan.Database.Poco
{
    public class Invoice : IEntity<int>
    {
        public int Id { get; set; }

        public Guid Code { get; set; }

        public Guid TenantCode { get; set; }

        /// <summary>
        /// Không được trùng giữa các công ty
        /// </summary>
        public string IdTransaction { get; set; }

        /// <summary>
        /// mã bảo mật để lưu key giải mã
        /// </summary>
        public string SecurityStemp { get; set; }

        /// <summary>
        /// file name xml ở trên minio
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// trạng thái gửi
        /// </summary>
        public int Status { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime CreatedUtc { get; set; }

        public string VerificationCode { get; set; }
    }
}
