﻿using EinvoiceTvan.MigrationElastic.Models;
using Infrastructure.Message.Rabbit;
using Nest;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace EinvoiceTvan.MigrationElastic.Worker
{
    public class InvoiceElasticSearchWorker : RabbitClient<InvoiceElasticInputModel, InvoiceElasticMessageModel>
    {
        private readonly IElasticClient _iElasticClient;
        public InvoiceElasticSearchWorker(IRabbitBus rabbitChannel,
                                          IElasticClient iElasticClient) : base(rabbitChannel)
        {
            _iElasticClient = iElasticClient;

            rabbitChannel.InitChannel("TvanSearch");
            BasicQos();
            InitQueue(queueName: "tvan-search-invoice");
            InitBinding(exchangeName: RabbitKey.Exchanges.Events, routingKeys: new List<string> {
                "SearchInvoice"
            });
        }

        public override async Task HandleAsync(BasicDeliverEventArgs ea, InvoiceElasticInputModel invoiceElasticInputModel)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(InvoiceTvanXmlWithoutCodeModel));
                StringReader strReader = new StringReader(invoiceElasticInputModel.XmlTvanSigned);
                InvoiceTvanXmlWithoutCodeModel data = (InvoiceTvanXmlWithoutCodeModel)serializer.Deserialize(strReader);

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(invoiceElasticInputModel.XmlTvanSigned);
                XmlNodeList x509IssuerNameList = doc.GetElementsByTagName("X509IssuerName");
                XmlNodeList x509SerialNumberList = doc.GetElementsByTagName("X509SerialNumber");

                string signatureTvan = "";
                string serialNumberTvan = "";

                if (x509IssuerNameList.Count > 0)
                {
                    signatureTvan = x509IssuerNameList[0].InnerXml;
                    signatureTvan = signatureTvan.Substring(4, signatureTvan.Length - 4);
                    signatureTvan = signatureTvan.Substring(0, signatureTvan.Length - 1);
                }

                if (x509SerialNumberList.Count > 0)
                {
                    serialNumberTvan = x509SerialNumberList[0].InnerXml;
                }

                var dataInvoiceElastic = new InvoiceTvanElasticModel();

                dataInvoiceElastic.IdTransaction = invoiceElasticInputModel.IdTransaction;
                dataInvoiceElastic.SignatureTvan = signatureTvan;
                dataInvoiceElastic.SerialNumberTvan = serialNumberTvan;

                if (data.DLieu.HDon.Count > 0)
                {
                    var ttHoaDon = new ThongTinHoaDonElastic();

                    foreach (var item in data.DLieu.HDon)
                    {
                        var ttThanhToan = new TToan();
                        var lstHHDVu = new List<HHDVu>();

                        ttHoaDon.TTChungHD = item.DLHDon.TTChung;
                        ttHoaDon.TTNBan = item.DLHDon.NDHDon.NBan;
                        ttHoaDon.TTNMua = item.DLHDon.NDHDon.NMua;

                        ttThanhToan.TgTCThue = item.DLHDon.NDHDon.TToan.TgTCThue;
                        ttThanhToan.TgTThue = item.DLHDon.NDHDon.TToan.TgTThue;
                        ttThanhToan.DSLPhi = item.DLHDon.NDHDon.TToan.DSLPhi;
                        ttThanhToan.TTCKTMai = item.DLHDon.NDHDon.TToan.TTCKTMai;
                        ttThanhToan.TgTTTBSo = item.DLHDon.NDHDon.TToan.TgTTTBSo;
                        ttThanhToan.TgTTTBChu = item.DLHDon.NDHDon.TToan.TgTTTBChu;
                        ttThanhToan.TTKhac = item.DLHDon.NDHDon.TToan.TTKhac;

                        ttHoaDon.TTTToan = ttThanhToan;

                        foreach (var hhdv in item.DLHDon.NDHDon.DSHHDVu.HHDVu)
                        {
                            lstHHDVu.Add(hhdv);
                        }

                        ttHoaDon.ListHHDVu = lstHHDVu;
                        dataInvoiceElastic.TTHoaDon = ttHoaDon;

                        await _iElasticClient.IndexDocumentAsync(dataInvoiceElastic);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            finally
            {
                BasicAck(ea);
            }
        }
    }
    
}
