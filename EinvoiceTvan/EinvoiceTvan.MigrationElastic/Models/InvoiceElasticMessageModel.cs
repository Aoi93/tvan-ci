﻿using EinvoiceTvan.MigrationElastic.Models;
using System.Collections.Generic;

namespace EinvoiceTvan.MigrationElastic.Models
{
    public class InvoiceElasticMessageModel
    {
        public string IdTransaction { get; set; }
        public string SignatureTvan { get; set; }
        public string SerialNumberTvan { get; set; }
        public TTHoaDonElastic TTHoaDon { get; set; }
    }

    public class TTHoaDonElastic
    {
        public TTChungDLHDon TTChungHD { get; set; }
        public NDHDonNBan TTNBan { get; set; }
        public NDHDonNMua TTNMua { get; set; }
        public TToan TTTToan { get; set; }
        public List<HHDVu> ListHHDVu { get; set; }
    }
}
