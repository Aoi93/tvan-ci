﻿using System.Collections.Generic;

namespace EinvoiceTvan.MigrationElastic.Models
{
    public class InvoiceTvanElasticModel 
    {
        public string IdTransaction { get; set; }
        public string SignatureTvan { get; set; }
        public string SerialNumberTvan { get; set; }
        public ThongTinHoaDonElastic TTHoaDon { get; set; }
    }

    public class ThongTinHoaDonElastic
    {
        public TTChungDLHDon TTChungHD { get; set; }
        public NDHDonNBan TTNBan { get; set; }
        public NDHDonNMua TTNMua { get; set; }
        public TToan TTTToan { get; set; }
        public List<HHDVu> ListHHDVu { get; set; }
    }
}
