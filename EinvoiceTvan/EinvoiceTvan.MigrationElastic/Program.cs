﻿using EinvoiceTvan.MigrationElastic.Worker;
using Infrastructure.Message.Rabbit;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Nest;
using NLog.Extensions.Logging;
using NLog.Web;
using System;
using System.Diagnostics;

namespace Einvoice.Tvan.MigrationElastic
{
    class Program
    {
        public static void Main(string[] args)
        {
            var pid = Process.GetCurrentProcess().Id;
            Console.WriteLine($"PID: {pid}");
            var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            try
            {
                logger.Debug($"PID: {pid}");
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                throw;
            }
            finally
            {
                NLog.LogManager.Shutdown();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .ConfigureServices(
                (hostContext, services) =>
                {
                    ConfigureServices(services, hostContext.Configuration);

                    services.AddHostedService<InvoiceElasticSearchWorker>();
                }
            );

        private static ServiceProvider ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.ClearProviders();
                loggingBuilder.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                loggingBuilder.AddNLog(configuration);
            });

            services.Configure<RabbitOption>(configuration.GetSection("RabbitMq"));
            services.AddSingleton<IRabbitBus, RabbitBus>();
            services.AddSingleton<IRabbitBusClient, RabbitBusClient>();
            AddElasticSearch(services, configuration.GetSection("Elasticsearch"));

            return services.BuildServiceProvider();
        }

        private static void AddElasticSearch(IServiceCollection services, IConfiguration configuration)
        {
            var url = configuration["Url"];
            var defaultIndex = configuration["Index"];
            var userName = configuration["Username"];
            var passWord = configuration["Password"];
            var settings = new ConnectionSettings(new Uri(url)).DefaultIndex(defaultIndex);
            settings.EnableHttpCompression();
            settings.BasicAuthentication(userName, passWord);
            var client = new ElasticClient(settings);
            services.AddSingleton<IElasticClient>(client);
        }
    }
}
