﻿using EinvoiceTvan.Database.Poco;
using Infrastructure.Database.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace EinvoiceTvan.Database.SqlServer
{
    public class EinvoiceTvanDbContext : DbContext, IStorageContext
    {
        public EinvoiceTvanDbContext(DbContextOptions<EinvoiceTvanDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Invoice>(builder =>
            {
                builder.ToTable("Invoices");
                builder.HasKey(x => x.Id);
                builder.Property(x => x.Id).IsRequired();

                builder.Property(x => x.FileName).IsRequired().IsUnicode(false).HasMaxLength(500);

                //Index
                builder.HasIndex(x => new { x.TenantCode, x.IdTransaction }).IsUnique();
                builder.Property(x => x.Code).IsRequired();
                builder.HasIndex(x => x.Code).IsUnique();
            });
        }
    }
}
