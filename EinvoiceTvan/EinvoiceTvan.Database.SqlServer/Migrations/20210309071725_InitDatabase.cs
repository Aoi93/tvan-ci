﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EinvoiceTvan.Database.SqlServer.Migrations
{
    public partial class InitDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Invoices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<Guid>(nullable: false),
                    TenantCode = table.Column<Guid>(nullable: false),
                    IdTransaction = table.Column<string>(nullable: true),
                    SecurityStemp = table.Column<string>(nullable: true),
                    FileName = table.Column<string>(unicode: false, maxLength: 500, nullable: false),
                    Status = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    CreatedUtc = table.Column<DateTime>(nullable: false),
                    VerificationCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invoices", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_Code",
                table: "Invoices",
                column: "Code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_TenantCode_IdTransaction",
                table: "Invoices",
                columns: new[] { "TenantCode", "IdTransaction" },
                unique: true,
                filter: "[IdTransaction] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Invoices");
        }
    }
}
