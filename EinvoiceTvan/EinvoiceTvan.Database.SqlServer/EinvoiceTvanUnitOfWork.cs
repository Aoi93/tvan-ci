﻿using Infrastructure.Database.Abstractions;
using Infrastructure.Database.EntityFramework;
using System;
using System.Collections.Generic;

namespace EinvoiceTvan.Database.SqlServer
{
    public class EinvoiceTvanUnitOfWork : EntityUnitOfWork<EinvoiceTvanDbContext>, IEinvoiceTvanUnitOfWork
    {
        public EinvoiceTvanUnitOfWork(IServiceProvider services, IEnumerable<IStorageContext> storageContexts) : base(services, storageContexts)
        {
        }
    }
}
