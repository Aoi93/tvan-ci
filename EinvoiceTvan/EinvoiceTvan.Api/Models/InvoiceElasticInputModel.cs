﻿namespace EinvoiceTvan.Api.Models
{
    public class InvoiceElasticInputModel
    {
        public string IdTransaction { get; set; }
        public string XmlTvanSigned { get; set; }
    }
}
