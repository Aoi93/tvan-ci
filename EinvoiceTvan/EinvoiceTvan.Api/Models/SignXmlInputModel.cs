﻿using EinvoiceTvan.Utils.Attributes;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace EinvoiceTvan.Api.Models
{
    public class SignXmlInputModel
    {
        [Required(ErrorMessage = "File hóa đơn không được để trống")]
        [FileAttribute(5, ErrorMessage = "Dung lượng file tối đa là 5MB")]
        public IFormFile File { get; set; }

        [Required(ErrorMessage = "Id Transaction không được để trống")]
        public string IdTransaction { get; set; }
    }
}
