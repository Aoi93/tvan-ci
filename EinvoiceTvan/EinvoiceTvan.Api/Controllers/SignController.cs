﻿using EinvoiceTvan.Api.Models;
using EinvoiceTvan.Api.Repository;
using EinvoiceTvan.Api.Services;
using EinvoiceTvan.Database;
using Infrastructure.Message.Rabbit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace EinvoiceTvan.Api.Controllers
{
    [Authorize]
    [Route("api/sign")]
    [ApiController]
    public class SignController 
    {
        private readonly IEinvoiceTvanUnitOfWork _tvanUoW;
        private readonly IRabbitService _rabbitService;
        private readonly IUploadToMinio _uploadToMinio;
        private readonly ISign _sign;

        public SignController(IEinvoiceTvanUnitOfWork tvanUoW,
                                 IRabbitService rabbitService,
                                 IUploadToMinio uploadToMinio,
                                 ISign sign
                                 )
        {
            _tvanUoW = tvanUoW;
            _rabbitService = rabbitService;
            _uploadToMinio = uploadToMinio;
            _sign = sign;
        }

        [Authorize]
        [HttpPost("xml")]
        public async Task<string> SignXmlAsync([FromBody]SignXmlInputModel input)
        {
            var invoiceRepository = _tvanUoW.GetRepository<IInvoiceRepository>();
            var fileName = input.File.FileName;

            using (var stream = new MemoryStream())
            {
                input.File.CopyTo(stream);
               
                var invoices = await invoiceRepository.GetEnvoiceAsync(input.IdTransaction);

                if (invoices != null)
                    return "Id Transaction này đã tồn tại!";

                var result = new StringBuilder();
               
                using (var reader = new StreamReader(input.File.OpenReadStream()))
                {
                    while (reader.Peek() >= 0)
                        result.AppendLine(await reader.ReadLineAsync());
                }
                var xml = result.ToString();

                var verifyXML = _sign.Verify(xml);

                if (verifyXML == false)
                    return "File không hợp lệ!";

                // tvan-sign
                var xmlTvanSigned = _sign.Sign(xml);

                var id = await _uploadToMinio.UploadFileToMinio(input.IdTransaction, fileName, xmlTvanSigned);

                _rabbitService.Publish(new InvoiceElasticInputModel
                {
                    IdTransaction = input.IdTransaction,
                    XmlTvanSigned = xmlTvanSigned
                }, RabbitKey.Exchanges.Events, "SearchInvoice");

                return xmlTvanSigned;
            }
        }

        /// <summary>
        /// fake data theo mẫu của TCT
        /// </summary>
        /// <returns></returns>
        [HttpPost("create-fake-data")]
        public string CreateFakeData()
        {
            var invoiceTvanXmlWithoutCodeModel = new InvoiceTvanXmlWithoutCodeModel();
            var ttChung = new TTChung();

            ttChung.PBan = "1.0.0";
            ttChung.MNGui = "0101352495999";
            ttChung.MNNhan = "010581529611";
            ttChung.MLTDiep = 200;
            ttChung.MTDiep = "0101352495998210000000002";
            ttChung.MTDTChieu = "0105815296210000000002";
            ttChung.MST = "0101352495999";
            ttChung.SLuong = "888";

            var dLieuWithoutCode = new DLieuWithoutCode();

            var lstHoaDon = new List<HDon>();
            var hoaDon = new HDon();

            var dlHDon = new DLHDon();

            var ttChungDLHDon = new TTChungDLHDon();

            ttChungDLHDon.PBan = "1.0.0";
            ttChungDLHDon.THDon = "HÓA ĐƠN THANH TOÁN ĐỒ DÙNG CÔNG NGHỆ";
            ttChungDLHDon.KHMSHDon = 1;
            ttChungDLHDon.KHHDon = "AA/17H";
            ttChungDLHDon.SHDon = 22;
            ttChungDLHDon.TDLap = DateTime.Now;
            ttChungDLHDon.DVTTe = "VND";
            ttChungDLHDon.TGia = 1;

            var ttKhac = new TTKhac();
            var ttin = new TTin();
            ttin.TTruong = "mã hàng hóa";
            ttin.KDLieu = "string";
            ttin.DLieu = "SP002";
            ttKhac.TTin = ttin;
            ttChungDLHDon.TTKhac = ttKhac;
            dlHDon.TTChung = ttChungDLHDon;
            var nDHDon = new NDHDon();
            var nBan = new NDHDonNBan();
            nBan.Ten = "Cửa hàng điện tử Cầu Giấy";
            nBan.MST = "1235679876543";
            nBan.DChi = "Số 5 ngõ 102 Nguyễn Đình Hoàn, Phường Nghĩa Đô, Quận Cầu Giấy, Thành phố Hà Nội";
            nDHDon.NBan = nBan;

            var nMua = new NDHDonNMua();
            nMua.Ten = "Nguyễn Hữu Đức";
            nMua.MST = "123567983456";
            nMua.DChi = "Số 6 ngõ 103 Nguyễn Đình Hoàn, Phường Nghĩa Đô, Quận Cầu Giấy, Thành phố Hà Nội";

            nDHDon.NMua = nMua;

            var dsHHDVu = new DSHHDVu();
            var lstHHDVu = new List<HHDVu>();
            var hhDVu = new HHDVu();
            hhDVu.TChat = 1;
            hhDVu.STT = 1;
            hhDVu.Ten = "Đồng hồ thông minh";
            hhDVu.DVTinh = "Chiếc";
            hhDVu.SLuong = 5;
            hhDVu.DGia = 2000000;
            hhDVu.TLCKhau = 0.5;
            hhDVu.STCKhau = 15000;
            hhDVu.ThTien = 10000000;
            hhDVu.TSuat = "1";

            //-----
            var hhDVu1 = new HHDVu();
            hhDVu1.TChat = 1;
            hhDVu1.STT = 1;
            hhDVu1.Ten = "Iphone 11 pro max";
            hhDVu1.DVTinh = "Chiếc";
            hhDVu1.SLuong = 10;
            hhDVu1.DGia = 34000000;
            hhDVu1.TLCKhau = 0.5;
            hhDVu1.STCKhau = 250000;
            hhDVu1.ThTien = 340000000;
            hhDVu1.TSuat = "1";
            lstHHDVu.Add(hhDVu);
            lstHHDVu.Add(hhDVu1);
            dsHHDVu.HHDVu = lstHHDVu;
            nDHDon.DSHHDVu = dsHHDVu;

            var ttToan = new TToan();
            var thTTLTSuat = new THTTLTSuat();
            var lstLTSuat = new List<LTSuat>();
            var ltSuat = new LTSuat();
            ltSuat.TSuat = "5";
            ltSuat.ThTien = 44000000;
            ltSuat.TThue = 50000;
            lstLTSuat.Add(ltSuat);
            thTTLTSuat.LTSuat = lstLTSuat;

            ttToan.THTTLTSuat = thTTLTSuat;
            ttToan.TgTCThue = 44000000;
            ttToan.TgTThue = 500000;
            ttToan.TTCKTMai = 1000000;
            ttToan.TgTTTBSo = 43000000;
            ttToan.TgTTTBChu = 43000000;

            nDHDon.TToan = ttToan;

            dlHDon.NDHDon = nDHDon;

            //dlHDon.DLQRCode = "qwsedrftgyuhijol8761234567890esdkjertyfcgmsdfumnbfghjoiuytre";

            hoaDon.DLHDon = dlHDon;
            //hoaDon.MCCQT = "qwertyuiopghjklkjhgfdio";

            lstHoaDon.Add(hoaDon);

            dLieuWithoutCode.HDon = lstHoaDon;
            invoiceTvanXmlWithoutCodeModel.TTChung = ttChung;
            invoiceTvanXmlWithoutCodeModel.DLieu = dLieuWithoutCode;


            using (var stringwriter = new StringWriter())
            {
                var serializer = new XmlSerializer(invoiceTvanXmlWithoutCodeModel.GetType());
                serializer.Serialize(stringwriter, invoiceTvanXmlWithoutCodeModel);
                var xml = stringwriter.ToString();

                var store = new X509Store(StoreLocation.CurrentUser);

                store.Open(OpenFlags.ReadOnly);

                var certificates = store.Certificates;

                var count = certificates.Count;
                return stringwriter.ToString();
            }
            //XmlDocument doc = new XmlDocument();
            //doc.LoadXml(content);
            //XmlNodeList elemList = doc.GetElementsByTagName("invoiceType");
            //if (elemList.Count > 0)
            //{
            //    invoiceType = elemList[0].InnerXml;
            //}
            //else
            //{
            //    invoiceType = "OtherInvoiceType";
            //}

            // convert data to model 
            //XmlNodeList signatureTag = doc.GetElementsByTagName("X509SubjectName");
            //string x509SubjectName = "";
            //if (signatureTag.Count > 0)
            //{
            //    x509SubjectName = signatureTag[0].InnerXml;
            //}
        }
    }
}
