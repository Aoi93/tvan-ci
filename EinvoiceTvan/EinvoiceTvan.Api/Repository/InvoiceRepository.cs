﻿using EinvoiceTvan.Database.Poco;
using Infrastructure.Database.Abstractions;
using Infrastructure.Database.EntityFramework;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace EinvoiceTvan.Api.Repository
{
    public interface IInvoiceRepository : IRepository<Invoice>
    {
        Task<Invoice> GetEnvoiceAsync(string idTransaction);
    }

    public class InvoiceRepository : EntityRepository<Invoice>, IInvoiceRepository
    {
        public async Task<Invoice> GetEnvoiceAsync(string idTransaction)
        {
            IQueryable<Invoice> query = DbSet.AsNoTracking().Where(x => x.IdTransaction == idTransaction);
            return await query.FirstOrDefaultAsync();
        }
    }
}
