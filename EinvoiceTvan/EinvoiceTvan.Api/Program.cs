using EinvoiceTvan.Api.Services;
using Infrastructure.Message.Rabbit;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using NLog.Web;
using System;
using System.Diagnostics;

namespace EinvoiceTvan.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var pid = Process.GetCurrentProcess().Id;
            Console.WriteLine($"PID: {pid}");
            var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            try
            {
                logger.Debug($"PID: {pid}");
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                throw;
            }
            finally
            {
                NLog.LogManager.Shutdown();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .ConfigureServices(
                    (hostContext, services) =>
                    {
                        ConfigureServices(services, hostContext.Configuration);
                    });

        private static ServiceProvider ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.ClearProviders();
                loggingBuilder.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                loggingBuilder.AddNLog(configuration);
            });

            services.Configure<RabbitOption>(configuration.GetSection("RabbitMq"));
            services.AddSingleton<IRabbitBus, RabbitBus>();
            services.AddSingleton<IRabbitBusClient, RabbitBusClient>();
            services.AddSingleton<IRabbitService, TvanBackgroundService>();

            return services.BuildServiceProvider();
        }
    }
}
