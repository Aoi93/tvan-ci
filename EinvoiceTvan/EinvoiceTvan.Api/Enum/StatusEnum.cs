﻿namespace EinvoiceTvan.Api.Enum
{
    public enum StatusEnum
    {
        /// <summary>
        /// Sent: Đã gửi cho TCT
        /// Unsend: Chưa gửi cho TCT
        /// Error: Đã gửi cho TCT nhưng bị lỗi
        /// </summary>
        Sent = 1,
        UnSend = 0,
        Error = -1
    }
}
