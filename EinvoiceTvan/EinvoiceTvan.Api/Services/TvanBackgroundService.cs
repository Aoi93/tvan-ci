﻿using Infrastructure.Message.Rabbit;

namespace EinvoiceTvan.Api.Services
{
    public class TvanBackgroundService : RabbitService, IRabbitService
    {
        public TvanBackgroundService(IRabbitBusClient busClient) : base(busClient)
        {
        }
    }
}
