﻿using EinvoiceTvan.Utils.DigitalSignature;
using Microsoft.Extensions.Configuration;
using System.Security.Cryptography.X509Certificates;

namespace EinvoiceTvan.Api.Services
{
    public interface ISign
    {
        string Sign(string xml);
        bool Verify(string xml);
    }

    public class SignService : ISign
    {
        private readonly IConfiguration _configuration;
        private readonly IDigitalSignatureService _digitalSignatureService;

        public SignService(IConfiguration configuration,
                               IDigitalSignatureService digitalSignatureService)
        {
            _configuration = configuration;
            _digitalSignatureService = digitalSignatureService;
        }

        public string Sign(string xml)
        {
            var signatureIdTvan = _configuration.GetSection("SignatureIdTvan:Id");
            var store = new X509Store(StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly);
            var certificates = store.Certificates;
            var xmlTvanSigned = _digitalSignatureService.Sign(certificates[0], xml, signatureIdTvan.Value);
            return xmlTvanSigned;
        }

        /// <summary>
        /// Verify Xml trước khi Tvan ký
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public bool Verify(string xml)
        {
            var verify = _digitalSignatureService.Verify(xml);
            foreach (var item in verify)
            {
                if (item.IsValid == false)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
