﻿using EinvoiceTvan.Api.Enum;
using EinvoiceTvan.Database;
using EinvoiceTvan.Database.Poco;
using EinvoiceTvan.Utils.Extensions;
using Infrastructure.File.Abstractions;
using Jarvis.Core.Services;
using System;
using System.Threading.Tasks;

namespace EinvoiceTvan.Api.Services
{
    public interface IUploadToMinio
    {
        Task<int> UploadFileToMinio(string idTransaction, string fileName, string xml);
    }

    public class UpLoadToMinioService : IUploadToMinio
    {
        private readonly IWorkContext _workContext;
        private readonly IFileService _fileService;
        private readonly IEinvoiceTvanUnitOfWork _tvanUoW;

        public UpLoadToMinioService(IWorkContext workContext,
                                    IFileService fileService,
                                    IEinvoiceTvanUnitOfWork tvanUoW)
        {
            _workContext = workContext;
            _fileService = fileService;
            _tvanUoW = tvanUoW;
        }

        /// <summary>
        /// 1. Lưu thông tin file upload vào DB
        /// 2. Mã hóa và upload File lên Minio
        /// </summary>
        /// <param name="idTransaction"></param>
        /// <param name="fileName"></param>
        /// <param name="stream"></param>
        /// <param name="xml"></param>
        /// <returns></returns>
        public async Task<int> UploadFileToMinio(string idTransaction, string fileName, string xml)
        {
            var invoiceRepo = _tvanUoW.GetRepository<Infrastructure.Database.Abstractions.IRepository<Invoice>>();
            var tenantCode = await _workContext.GetTenantCodeAsync();
            var securityStemp = CrytographyExtension.AES_GenerateKey(256);

            var invoiceEntity = new Invoice
            {
                Code = Guid.NewGuid(),
                TenantCode = tenantCode,
                IdTransaction = idTransaction,
                Status = (int)StatusEnum.UnSend,
                CreatedAt = DateTime.Now,
                CreatedUtc = DateTime.UtcNow,
                SecurityStemp = securityStemp,
                FileName = fileName
            };

            byte[] key = Convert.FromBase64String(securityStemp);

            var now = DateTime.Now;
            var aes_ComputeIV = CrytographyExtension.AES_ComputeIV(key, key.Length / 2);
            var aes_Encrypt = CrytographyExtension.AES_Encrypt(xml, key, aes_ComputeIV);
            var filePathMinio = $"{tenantCode}/{now.Year}/{now.Month.ToString("D2")}/{now.Day.ToString("D2")}/{now.Hour.ToString("D2")}/{invoiceEntity.Code}" + ".xml";

            await _fileService.UploadAsync(filePathMinio, aes_Encrypt);
            await invoiceRepo.InsertAsync(invoiceEntity);
            await _tvanUoW.CommitAsync();

            return invoiceEntity.Id;
        }
    }
}
