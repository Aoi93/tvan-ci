using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace EinvoiceTvan.Utils.DigitalSignature
{
    public interface IDigitalSignatureService
    {
        string Sign(X509Certificate2 certificate, string xml, string signatureId);

        List<CertificateModel> Verify(string xml);
    }
}