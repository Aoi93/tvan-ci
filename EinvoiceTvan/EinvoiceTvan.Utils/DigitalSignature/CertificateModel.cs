using System;

namespace EinvoiceTvan.Utils.DigitalSignature
{
    public class CertificateModel
    {
        public bool IsValid { get; set; }
        public string CommonName { get; set; }
        public string Locality { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public string Issuer { get; set; }
        public string SerialNumber { get; set; }
        public DateTime? SignedAt { get; set; }
    }
}