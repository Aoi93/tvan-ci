using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Xml;
using EinvoiceTvan.Utils.Extensions;

namespace EinvoiceTvan.Utils.DigitalSignature
{
    public class DigitalSignatureService : IDigitalSignatureService
    {
        public string Sign(X509Certificate2 certificate, string xml, string signatureId)
        {
            // Create a new XML document.
            XmlDocument document = new XmlDocument();

            // Load an XML file into the XmlDocument object.
            document.PreserveWhitespace = true;
            document.LoadXml(xml);

            // Sign the XML document. 
            AddSignature(document, certificate, signatureId);
            return document.OuterXml;
        }

        private static void AddSignature(XmlDocument document, X509Certificate2 certificate, string signatureId)
        {
            // var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            var signedXml = new SignedXml(document);
            signedXml.Signature.Id = signatureId;
            signedXml.SigningKey = certificate.PrivateKey;

            var keyInfo = new KeyInfo();

            var keyInfoData = new KeyInfoX509Data(certificate);

            var xserial = new X509IssuerSerial();
            xserial.IssuerName = certificate.IssuerName.Name;
            xserial.SerialNumber = certificate.SerialNumber;
            keyInfoData.AddIssuerSerial(xserial.IssuerName, xserial.SerialNumber);
            keyInfoData.AddSubjectName(certificate.SubjectName.Name);

            keyInfo.AddClause(keyInfoData);
            keyInfo.Id = DateTimeOffset.UtcNow.ToEpochTime().ToString();

            signedXml.KeyInfo = keyInfo;

            var reference = new Reference("#data");
            var env = new XmlDsigEnvelopedSignatureTransform();
            reference.AddTransform(env);
            signedXml.AddReference(reference);

            signedXml.ComputeSignature();

            var xmlDigitalSignature = signedXml.GetXml();

            var verify = signedXml.CheckSignature();

            // Append the element to the XML document.
            document.DocumentElement.AppendChild(document.ImportNode(xmlDigitalSignature, true));
        }

        public List<CertificateModel> Verify(string xml)
        {
            XmlDocument document = new XmlDocument();

            // Load an XML file into the XmlDocument object.
            document.PreserveWhitespace = true;
            document.LoadXml(xml);

            // Create a new SignedXml object and pass it
            // the XML document class.
            SignedXml signedXml = new SignedXml(document);

            // Find the "Signature" node and create a new

            XmlNodeList signatures = document.GetElementsByTagName("Signature");

            // Throw an exception if no signature was found.
            var certs = new List<CertificateModel>();
            if (signatures.Count <= 0)
                return certs;

            foreach (XmlElement signature in signatures)
            {
                // Load the first <signature> node.
                signedXml.LoadXml(signature);

                //Certificate info
                CertificateModel info = GetCertificateInfo(signedXml, signature);

                //Sign time
                var signedAt = GetSignTime(signature);
                if (signedAt.HasValue)
                    info.SignedAt = signedAt.Value.LocalDateTime;

                certs.Add(info);
            }
            return certs;
        }

        private static DateTimeOffset? GetSignTime(XmlElement signature)
        {
            var tags = signature.GetElementsByTagName("KeyInfo");
            if (tags.Count == 0)
                return null;

            var id = tags[0].Attributes["Id"];
            if (id == null)
                return null;

            var timestamp = long.Parse(id.Value);
            var time = timestamp.ToDateTimeOffsetFromEpoch();
            return time;
        }

        private static CertificateModel GetCertificateInfo(SignedXml signedXml, XmlElement signature)
        {
            var x509s = signature.GetElementsByTagName("X509Certificate");
            var certificate = x509s[0].InnerText;
            byte[] bytes = Convert.FromBase64String(certificate);
            var cert = new X509Certificate2(bytes);
            var info = new CertificateModel
            {
                IsValid = signedXml.CheckSignature(),
                ValidFrom = cert.NotBefore,
                ValidTo = cert.NotAfter,
                SerialNumber = cert.SerialNumber
            };

            GetSubjectInfo(cert, info);
            GetIssuerInfo(cert, info);

            return info;
        }

        private static void GetIssuerInfo(X509Certificate2 cert, CertificateModel info)
        {
            string[] splitedIssuer;
            splitedIssuer = cert.Issuer.Split(", ");
            // if (cert.Issuer.Contains(","))
            //     splitedIssuer = cert.Issuer.Split(", ");
            // else
            //     splitedIssuer = cert.Issuer.Split(" ");

            foreach (var item in splitedIssuer)
            {
                if (item.StartsWith("CN="))
                    info.Issuer += item.Substring(3, item.Length - 3) + ", ";

                if (item.StartsWith("O="))
                    info.Issuer += item.Substring(2, item.Length - 2);
            }
        }

        private static void GetSubjectInfo(X509Certificate2 cert, CertificateModel info)
        {
            string[] splitedSubject;
            splitedSubject = cert.Subject.Split(", ");
            // if (cert.Subject.Contains(","))
            //     splitedSubject = cert.Subject.Split(", ");
            // else
            //     splitedSubject = cert.Subject.Split(" ");

            foreach (var item in splitedSubject)
            {
                if (item.StartsWith("C="))
                    info.Country = item.Substring(2, item.Length - 2);

                if (item.StartsWith("S="))
                    info.State = item.Substring(2, item.Length - 2);

                if (item.StartsWith("L="))
                    info.Locality = item.Substring(2, item.Length - 2);

                if (item.StartsWith("CN="))
                    info.CommonName = item.Substring(3, item.Length - 3);
            }
        }
    }
}