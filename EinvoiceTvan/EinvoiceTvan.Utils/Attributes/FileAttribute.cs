﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace EinvoiceTvan.Utils.Attributes
{
    public class FileAttribute : ValidationAttribute
    {
        private readonly int _otherProperty;
        public FileAttribute(int otherProperty)
        {
            _otherProperty = otherProperty;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
                return ValidationResult.Success;

            var file = (IFormFile)value;

            using (var stream = new MemoryStream())
            {
                file.CopyTo(stream);

                var fileLength = _otherProperty * 1024 * 1024;

                // stream.Length trả về độ dài file là Byte
                if (stream.Length > fileLength)
                    return new ValidationResult(ErrorMessage = $"Dung lượng file tối đa {_otherProperty}MB");
            }

            return ValidationResult.Success;
        }
    }
}
