I. Chức năng
---
1. Đăng nhập (account)
2. Đăng xuất
3. Đăng ký
4. Đặt lại mật khẩu
5. Thông tin tenant
6. Quản lý tenant (CRUD)
7. Quản lý tài khoản (CRUD, gán quyền, khoá/mở)
8. Quản lý quyền (CRUD, phân quyền)
9. Quản lý nhãn (CRUD)
10. Quản lý tham số hệ thống (RU)

II. Kiến trúc
1. 