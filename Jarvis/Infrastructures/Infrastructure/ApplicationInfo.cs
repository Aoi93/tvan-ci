﻿namespace Infrastructure
{
    public class ApplicationInfo
    {
        public string Name { get; set; }
        public string Version { get; set; }
        public string JarvisFolder { get; set; }
        public string ModuleFolder { get; set; }
    }
}
