﻿namespace Jarvis.Core.Permissions
{
    public class SpecialPolicy
    {
        public const string Special_DoEnything = "Tất cả quyền";
        public const string Special_TenantAdmin = "Quản lý tenant";
        public const string Special_OrganizationAdmin = "Quản lý phòng/ban";
    }
}
